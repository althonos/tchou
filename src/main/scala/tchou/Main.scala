package tchou

import javax.swing.UIManager
import scala.xml.XML

import uk.me.jstott.jcoord.LatLng

import tchou.core.eco.City
import tchou.core.Game
import tchou.core.network.Track
import tchou.ui.windows.GameWindow



object Main extends App {

    // Make the UI less ugly
    try {
      UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
    } catch {
      case _: Throwable => ()
    }

    // Requires Scala < 2.12, or does not work
    System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer")

    // Create a default game
    val game = Game.readXml( XML.load(getClass().getResourceAsStream("maps/france.xml")) )

    // Create a new window
    val window = new GameWindow(game)

    while (true) {
        Thread.sleep(1000)
        window.game.update()
    }

}
