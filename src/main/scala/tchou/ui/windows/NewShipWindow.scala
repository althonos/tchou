package tchou.ui.windows

import java.awt.BorderLayout
import java.awt.GridLayout
import java.awt.Dimension

import javax.swing.Box
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.JLabel
import javax.swing.JComboBox
import javax.swing.JButton
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.BoxLayout
import javax.swing.JScrollPane
import javax.swing.ScrollPaneConstants
import javax.swing.ListSelectionModel
import javax.swing.event.ListSelectionListener
import javax.swing.event.ListSelectionEvent

import scala.collection.immutable.Seq
import scala.collection.mutable.Buffer
import scala.collection.mutable.ArrayBuffer

import tchou.core.Game
import tchou.core.eco.City
import tchou.core.eco.cargo.Cargo
import tchou.core.network.Station
import tchou.core.network.Network
import tchou.core.vehicles.ships.Ship
import tchou.core.vehicles.ships.ShipClass

import tchou.ui.widgets.GameListView
import tchou.ui.widgets.ItinerarySelector


/* FIXME: Train model changed. */
class NewShipWindow(val game: Game, val vehiclesWindow: VehiclesWindow)
  extends JFrame
  with ActionListener {
    /* TODO: proper selector for train itinerary */
    /* TODO: prettier UI using a GridBagLayout instead of a GridLayout */


    val classComboBox: JComboBox[String] = new JComboBox()
    val cargoComboBox: JComboBox[String] = new JComboBox()
    val itinerarySelector = new ItinerarySelector(game) {
      def network(game: Game): Network = game.networks.water
    }

    val okButton = new JButton("Ok")
    val cancelButton = new JButton("Cancel")

    //
    ShipClass.instances.foreach(x => classComboBox.addItem(x._1))
    okButton.addActionListener(this)
    cancelButton.addActionListener(this)
    classComboBox.addActionListener(this)

    //
    setSize(720, 480)
    setTitle("Tchou:New Ship...")
    makeLayout()
    setVisible(true)

    private def makeLayout() = {
      val box = Box.createVerticalBox()

      val cls = Box.createHorizontalBox()
      cls.add(new JLabel("Class: "))
      cls.add(classComboBox)
      box.add(cls)

      val resource = Box.createHorizontalBox()
      resource.add(new JLabel("Cargo: "))
      resource.add(cargoComboBox)
      box.add(resource)

      val it = Box.createVerticalBox()
      itinerarySelector.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE))
      it.add(new JLabel("Itinerary: "))
      it.add(itinerarySelector)
      box.add(it)

      val buttons = Box.createHorizontalBox()
      okButton.setMaximumSize(new Dimension(100, 30))
      cancelButton.setMaximumSize(new Dimension(100, 30))
      buttons.add(okButton)
      buttons.add(cancelButton)
      box.add(buttons)

      this.add(box)
    }

    def cls: ShipClass = {
      ShipClass.instances(classComboBox.getItemAt(classComboBox.getSelectedIndex()))
    }

    def resource: Cargo = {
      Cargo.instances(cargoComboBox.getItemAt(cargoComboBox.getSelectedIndex()))
    }

    def itinerary: Seq[Station] = {
      itinerarySelector.selected.to[Seq]
    }

    def actionPerformed(e: ActionEvent) = {
        e.getSource() match {
            case `classComboBox` => {
              cargoComboBox.removeAllItems()
              cls.accepts.foreach(x => cargoComboBox.addItem(x.name))
            }
            // The Cancel button was pressed
            case `cancelButton` => {
                setVisible(false)
                dispose()
            }
            // The Ok button was pressed
            case `okButton` => {
              if (itinerary.size > 1) {
                game.addVehicle(new Ship(cls, resource, itinerary))
                vehiclesWindow.tabs.ships.updateList()
                setVisible(false)
                dispose()
              }
            }
            // Something else: ignore
            case _ => ()
        }
    }

}
