package tchou.ui.windows

import java.awt.BorderLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener

import javax.swing.Box
import javax.swing.BoxLayout
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JList
import javax.swing.JButton
import javax.swing.Timer
import javax.swing.JScrollPane
import javax.swing.ScrollPaneConstants
import javax.swing.ListSelectionModel
import javax.swing.event.ListSelectionListener
import javax.swing.event.ListSelectionEvent

import scala.collection.mutable.HashMap
import scala.collection.mutable.Map

import tchou.core.Game
import tchou.core.eco.City
import tchou.core.eco.cargo.Cargo

import tchou.ui.widgets.GameListView


sealed class ResourcesPopup(city: City)
  extends JFrame
  with ActionListener {

    val box = Box.createHorizontalBox()
    val keys = Box.createVerticalBox()
    val values = Box.createVerticalBox()

    private val labelsMap: HashMap[Cargo, JLabel] = new HashMap()
    private val statusUpdater = new Timer(500, this)
    statusUpdater.start()

    makeLayout()
    setSize(120, 360)
    setTitle(f"Resources (${city.name})")
    setVisible(true)

    private def makeLayout() = {
      city.resources.toSeq.sortBy(_._1.name).foreach(r => {
        keys.add(new JLabel(f"<html><b>${r._1.name}: </b></html>"))
        labelsMap(r._1) = new JLabel(f"${r._2}")
        values.add(labelsMap(r._1))
      })
      box.add(keys)
      box.add(values)
      add(box)
    }

    private def updateInfo() = {
      city.resources.toSeq.foreach(r => labelsMap(r._1).setText(f"${r._2}"))
    }

    def actionPerformed(e: ActionEvent) = ResourcesPopup.this.updateInfo()
}


/* TODO: maybe init from List[Trains] instead of Game */
final class CitiesWindow(game: Game)
  extends JFrame
  with ActionListener
  with ListSelectionListener {

    private val citiesView = new GameListView[City] {override def stringify(c: City) = c.name}
    private val statusUpdater = new Timer(100, this)
    private var _selected: Option[City] = None

    lazy val latitudeLabel = new JLabel(" ")
    lazy val longitudeLabel = new JLabel(" ")
    lazy val popLabel = new JLabel(" ")
    lazy val trainStationLabel = new JLabel(" ")
    lazy val portLabel = new JLabel(" ")
    lazy val industriesLabel = new JLabel(" ")
    lazy val resourcesButton = new JButton("Show...")

    //
    setSize(480, 360)
    setTitle("Tchou:Cities")
    setVisible(false)
    makeLayout()
    updateInfo()

    // Setup the displayed list
    citiesView.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
    citiesView.setListData(game.cities.map(_._2).toSeq.sortBy(_.name))
    citiesView.getSelectionModel().addListSelectionListener(this)

    //
    resourcesButton.addActionListener(this)
    statusUpdater.start()

    def selected = _selected
    def selected_=(c: City): Unit = {
        selected = Some(c)
    }
    def selected_=(c: Option[City]): Unit = {
        _selected.map(city => Seq(game.networks.rail, game.networks.water).map(x => x.stations.get(city.name).map(s => s.node.removeAttribute("ui.selected"))))
        _selected = c
        _selected.map(city => Seq(game.networks.rail, game.networks.water).map(x => x.stations.get(city.name).map(s => s.node.addAttribute("ui.selected"))))
    }

    private def makeLayout() {
        setLayout(new BorderLayout())

        // Top bar label
        add(new JLabel("Cities: "), BorderLayout.NORTH)

        // City list
        citiesView.setFixedCellWidth(420)
        citiesView.setFixedCellHeight(20)
        citiesView.setVisibleRowCount(-1)

        val cityScroller = new JScrollPane(citiesView)
        cityScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS)
        add(cityScroller, BorderLayout.CENTER)

        // City info
        val info = Box.createHorizontalBox()
        val keys = Box.createVerticalBox()
        val values = Box.createVerticalBox()

        keys.add(new JLabel("Latitude: "))
        keys.add(new JLabel("Longitude: "))
        keys.add(new JLabel("Population: "))
        keys.add(new JLabel("Train Station: "))
        keys.add(new JLabel("Port: "))
        keys.add(new JLabel("Factories: "))
        keys.add(new JLabel("Resources: "))

        values.add(latitudeLabel)
        values.add(longitudeLabel)
        values.add(popLabel)
        values.add(trainStationLabel)
        values.add(portLabel)
        values.add(industriesLabel)
        values.add(resourcesButton)

        info.add(Box.createHorizontalStrut(5))
        info.add(keys)
        info.add(Box.createHorizontalStrut(10))
        info.add(values)
        add(info, BorderLayout.SOUTH)
    }

    def updateInfo() {
        citiesView.getSelectedItem() match {
            case Some(city) => {
                latitudeLabel.setText(f"${city.coordinates.getLat()}")
                longitudeLabel.setText(f"${city.coordinates.getLng()}")
                popLabel.setText(f"${city.size}")
                trainStationLabel.setText(
                  if (game.networks.rail.stations.contains(city.name)) {"Yes"} else {"No"})
                portLabel.setText(
                  if (game.networks.water.stations.contains(city.name)) {"Yes"} else {"No"})
                industriesLabel.setText(
                  if (city.industries.isEmpty) {" "} else {city.industries.map(_.name).mkString(", ")})
            }
            case None => {
                latitudeLabel.setText(" ")
                longitudeLabel.setText(" ")
                popLabel.setText(" ")
                trainStationLabel.setText(" ")
                portLabel.setText(" ")
                industriesLabel.setText(" ")
            }
        }
    }

    def setSelectedCity(name: String): Unit = {
        setSelectedCity(game.cities(name))
    }

    def setSelectedCity(city: City): Unit = {
      val index = citiesView.indexOf(city)
      citiesView.setSelectedIndex(index)
    }

    override def setVisible(on: Boolean) = {
      citiesView.clearSelection()
      selected = None
      super.setVisible(on)
    }

    def actionPerformed(e: ActionEvent) = {
      e.getSource() match {
        case `resourcesButton` => selected.map(c => new ResourcesPopup(c));
        case _ => updateInfo();
      }
    }

    def valueChanged(e: ListSelectionEvent) = {
      selected = citiesView.getSelectedItem()
    }

}
