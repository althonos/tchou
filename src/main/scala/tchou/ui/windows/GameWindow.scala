package tchou.ui.windows

import java.awt.BorderLayout
import javax.swing.JFrame
import javax.swing.JMenuBar

import tchou.core.Game
import tchou.ui.widgets.GameConsole
import tchou.ui.widgets.GameMenuBar
import tchou.ui.widgets.GameToolbar
import tchou.ui.widgets.GameStatusBar
import tchou.ui.widgets.GameView


final class GameWindow(g: Game) extends JFrame() {

    def this() {
        this(new Game())
    }

    /** The underlying game instance
     */
    private var _game = g

    // Game window components
    private var _citiesWindow = new CitiesWindow(game)
    private var _menubar = new GameMenuBar(this)
    private var _toolbar = new GameToolbar(this)
    private var _vehiclesWindow = new VehiclesWindow(game)
    private var _view = new GameView(this)
    private var _status = new GameStatusBar(game)

    // Read-only accessors
    def citiesWindow = _citiesWindow
    def menubar = _menubar
    def toolbar = _toolbar
    def vehiclesWindow = _vehiclesWindow
    def view = _view
    def status = _status
    def game: Game = _game

    // Finish initialisation
    setTitle("Tchou")
    setSize(1280, 720)
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    setLocationRelativeTo(null)
    setLayout(new BorderLayout())
    game = g

    // Setting the game refreshes the widgets
    def game_=(game: Game) = {

      // Update game instance
      _game = game

      getContentPane().remove(view)

      // Refresh widgets
      _citiesWindow = new CitiesWindow(game)
      _menubar = new GameMenuBar(this)
      _toolbar = new GameToolbar(this)
      _vehiclesWindow = new VehiclesWindow(game)
      _view = new GameView(this)
      _status = new GameStatusBar(game)

      // Add widgets to self
      // getContentPane().add(console, BorderLayout.SOUTH)
      getContentPane().add(toolbar, BorderLayout.WEST)
      getContentPane().add(view, BorderLayout.CENTER)
      getContentPane().add(status, BorderLayout.NORTH)

      // initialize menu bar
      setJMenuBar(menubar)

      // Display the frame, but not the togglable components
      setVisible(true)
      // console.setVisible(false)
      citiesWindow.setVisible(false)
      vehiclesWindow.setVisible(false)
    }

}
