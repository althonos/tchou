package tchou.ui.windows

import java.awt.BorderLayout
import java.awt.GridLayout
import java.awt.Dimension

import javax.swing.Box
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.JLabel
import javax.swing.JComboBox
import javax.swing.JButton
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.BoxLayout
import javax.swing.JScrollPane
import javax.swing.ScrollPaneConstants
import javax.swing.ListSelectionModel
import javax.swing.event.ListSelectionListener
import javax.swing.event.ListSelectionEvent

import scala.collection.immutable.Seq
import scala.collection.mutable.Buffer
import scala.collection.mutable.ArrayBuffer

import tchou.core.eco.City
import tchou.core.Game
import tchou.core.network.Station
import tchou.core.network.Network
import tchou.core.vehicles.trains.Train
import tchou.core.vehicles.trains.Engine
import tchou.core.vehicles.trains.Carriage

import tchou.ui.widgets.GameListView
import tchou.ui.widgets.ItinerarySelector


sealed class CarriageSelector(game: Game)
  extends Box(BoxLayout.X_AXIS)
  with ActionListener {

      private val _selected: Buffer[Carriage] = new ArrayBuffer()

      private val selectedCarriages = new GameListView[Carriage] {
        override def stringify(c: Carriage) = {
          f"<html><b>${c.name}</b> (${c.cargo.name} x${c.capacity})"
        }
      }

      private val availableCarriages = new GameListView[Carriage] {
        override def stringify(c: Carriage) = {
          f"<html><b>${c.name}</b> (${c.cargo.name} x${c.capacity})"
        }
      }

      lazy val addButton = new JButton("+")
      lazy val rmButton = new JButton("-")
      addButton.addActionListener(this)
      rmButton.addActionListener(this)

      makeLayout()
      setVisible(true)

      private def makeLayout() = {
        // val box = Box.createHorizontalBox()
        // box.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE))

        val selectedScroller = new JScrollPane(selectedCarriages)
        selectedScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS)
        selectedScroller.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE))
        // box.add(selectedScroller)
        add(selectedScroller)

        val buttons = Box.createVerticalBox()
        addButton.setMaximumSize(new Dimension(40, addButton.getMinimumSize().height));
        rmButton.setMaximumSize(new Dimension(40, rmButton.getMinimumSize().height));
        buttons.add(addButton)
        buttons.add(rmButton)
        // box.add(buttons)
        add(buttons)

        val availableScroller = new JScrollPane(availableCarriages)
        availableScroller.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE))
        availableScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS)
        // box.add(availableScroller)
        add(availableScroller)

        availableCarriages.setListData(Carriage.instances.map(_._2).toList.sortBy(_.name))
        // this.add(box)
      }

      def actionPerformed(e: ActionEvent) {
          e.getSource() match {
              case `rmButton` => {
                selectedCarriages.getSelectedItem() match {
                  case Some(c) => {_selected -= c}
                  case _ => {}
                }
                selectedCarriages.setListData(_selected)
              }
              case `addButton` => {
                availableCarriages.getSelectedItem() match {
                  case Some(c) => {_selected += c}
                  case _ => {}
                }
                selectedCarriages.setListData(_selected)
              }
              case _ => {}
          }
      }

      def selected: Buffer[Carriage] = _selected

}


class NewTrainWindow(val game: Game, val vehiclesWindow: VehiclesWindow)
  extends JFrame
  with ActionListener {
    /* TODO: proper selector for train itinerary */
    /* TODO: prettier UI using a GridBagLayout instead of a GridLayout */

    val engineComboBox: JComboBox[String] = new JComboBox()
    val carriageSelector = new CarriageSelector(game)
    val itinerarySelector = new ItinerarySelector(game) {
      def network(game: Game): Network = game.networks.rail
    }

    val okButton = new JButton("Ok")
    val cancelButton = new JButton("Cancel")

    // Initialize components properties and contents
    Engine.instances.foreach(x => engineComboBox.addItem(x._1))
    okButton.addActionListener(this)
    cancelButton.addActionListener(this)

    // Initialize frame properties
    setSize(720, 480)
    setTitle("Tchou:New Train...")
    makeLayout()
    setVisible(true)

    private def makeLayout() = {
      val box = Box.createVerticalBox()

      // Add components to the frame
      val engine = Box.createHorizontalBox()
      engine.add(new JLabel("Engine: "))
      engine.add(engineComboBox)
      box.add(engine)

      val carriage = Box.createVerticalBox()
      carriageSelector.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE))
      carriage.add(new JLabel("Carriage: "))
      carriage.add(carriageSelector)
      box.add(carriage)

      val it = Box.createVerticalBox()
      itinerarySelector.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE))
      it.add(new JLabel("Itinerary: "))
      it.add(itinerarySelector)
      box.add(it)

      val buttons = Box.createHorizontalBox()
      okButton.setMaximumSize(new Dimension(100, 30))
      cancelButton.setMaximumSize(new Dimension(100, 30))

      buttons.add(okButton)
      buttons.add(cancelButton)
      box.add(buttons)

      this.add(box)
    }

    def engine: Engine = {
      Engine.instances(engineComboBox.getItemAt(engineComboBox.getSelectedIndex()))
    }

    def carriages: Seq[Carriage] = {
      carriageSelector.selected.to[Seq]
    }

    def itinerary: Seq[Station] = {
      itinerarySelector.selected.to[Seq]
    }

    def actionPerformed(e: ActionEvent) = {
        e.getSource() match {
            // The Cancel button was pressed
            case `cancelButton` => {
                setVisible(false)
                dispose()
            }
            // The Ok button was pressed
            case `okButton` => {
              if (itinerary.size > 1) {
                game.addVehicle(new Train(engine, carriages, itinerary))
                vehiclesWindow.tabs.trains.updateList()
                setVisible(false)
                dispose()
              }
            }
            // Something else: ignore
            case _ => ()
        }
    }

}
