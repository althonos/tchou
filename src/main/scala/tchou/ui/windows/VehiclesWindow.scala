package tchou.ui.windows

import java.awt.BorderLayout
import java.awt.GridLayout
import java.awt.GridBagLayout
import java.awt.GridBagConstraints
import java.awt.Dimension
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.WindowEvent
import java.awt.event.WindowAdapter
import java.awt.event.WindowListener

import javax.swing.Box
import javax.swing.Timer
import javax.swing.JPanel
import javax.swing.JLabel
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JList
import javax.swing.BoxLayout
import javax.swing.JTabbedPane
import javax.swing.JScrollPane
import javax.swing.ScrollPaneConstants
import javax.swing.ListSelectionModel
import javax.swing.SwingConstants
import javax.swing.event.ListSelectionListener
import javax.swing.event.ListSelectionEvent

import scala.collection.JavaConverters._

import tchou.core.Game
import tchou.core.eco.City
import tchou.core.network.Station
import tchou.core.network.Track
import tchou.core.vehicles.Vehicle
import tchou.core.vehicles.trains.Train
import tchou.core.vehicles.ships.Ship

import tchou.ui.widgets.GameListView



sealed abstract class VehiclePane[V <: Vehicle](
    game: Game,
    val name: String,
    vehiclesWindow: VehiclesWindow)
  extends JPanel
  with ActionListener
  with ListSelectionListener {

    private val vehiclesView = new GameListView[V] {
      override def stringify(v: V) = {
        val it = v.itinerary.map(_.city.name).mkString("-")
        f"<html><b>${v.id}</b> ($it)</html>"
      }
    }

    lazy val newButton = new JButton("+")
    lazy val rmButton = new JButton("-")
    private val statusUpdater = new Timer(500, this)

    protected val costLabel = new JLabel(" ")
    protected val positionLabel = new JLabel(" ")
    protected val speedLabel = new JLabel(" ")
    protected val capacityLabel = new JLabel(" ")
    protected val loadLabel = new JLabel(" ")

    newButton.addActionListener(this)
    rmButton.addActionListener(this)
    makeLayout()
    updateList()
    statusUpdater.start()


    protected def makeLayout() = {
      setLayout(new BorderLayout())

      val status = new JPanel()
      status.setLayout(new GridLayout(1, 4))
      status.add(new JLabel(f"<html><b>$name: </b></html>"))
      status.add(new JPanel())
      status.add(newButton)
      status.add(rmButton)
      add(status, BorderLayout.NORTH)

      vehiclesView.setFixedCellWidth(420)
      vehiclesView.setFixedCellHeight(20)
      vehiclesView.setVisibleRowCount(-1)

      val vehiclesScroller = new JScrollPane(vehiclesView)
      vehiclesScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS)
      add(vehiclesScroller, BorderLayout.CENTER)

      val info = Box.createHorizontalBox()
      val keys = Box.createVerticalBox()
      val values = Box.createVerticalBox()
      keys.add(new JLabel("Cost: "))
      keys.add(new JLabel("Position: "))
      keys.add(new JLabel("Speed: "))
      keys.add(new JLabel("Capacity: "))
      keys.add(new JLabel("Load: "))
      values.add(costLabel)
      values.add(positionLabel)
      values.add(speedLabel)
      values.add(capacityLabel)
      values.add(loadLabel)
      info.add(keys)
      info.add(values)
      add(info, BorderLayout.SOUTH)
    }

    def vehicleList(): Seq[V]
    protected def newWindow(game: Game, w: VehiclesWindow)

    def updateInfo(selected: Option[V]): Unit = {
      selected match {
        case Some(vehicle) => {
          costLabel.setText(f"${vehicle.cost}")
          speedLabel.setText(f"${vehicle.speed}")

          val capacityString = vehicle.capacity.map(x => f"${x._1.name} x${x._2}").mkString(", ")
          capacityLabel.setText(if (!capacityString.isEmpty) {f"$capacityString"} else {" "})

          val loadString = vehicle.load.map(x => f"${x._1.name} x${x._2}").mkString(", ")
          loadLabel.setText(if (!loadString.isEmpty) {f"$loadString"} else {" "})

          vehicle.location match {
            case s: Station => {
              positionLabel.setText(f"${s.city.name}")
            }
            case t: Track => {
              val src = vehicle.roadmap.head
              val dst = t.getOpposite(src)
              positionLabel.setText(f"Between ${src.city.name} and ${dst.city.name} (${vehicle.left}km left)")
            }
            case _ => {}
          }
        }
        case None => {
          costLabel.setText(" ")
          positionLabel.setText(" ")
          speedLabel.setText(" ")
          capacityLabel.setText(" ")
        }
      }
    }

    def updateList(): Unit = {
      vehiclesView.setListData(vehicleList())
    }

    def actionPerformed(e: ActionEvent) = {
      updateInfo(vehiclesView.getSelectedItem())
      e.getSource() match {
        case `rmButton` => {
          vehiclesView.getSelectedItem().map(game.removeVehicle(_))
          updateList()
        }
        case `newButton` => {
          newWindow(game, vehiclesWindow)
        }
        case _ => {}
      }
    }

    def valueChanged(e: ListSelectionEvent) = {
      updateInfo(vehiclesView.getSelectedItem())
    }

}



sealed class TrainsPane(game: Game, w: VehiclesWindow)
  extends VehiclePane[Train](game, "Trains", w) {

    def newWindow(game: Game, w: VehiclesWindow) = new NewTrainWindow(game, w)
    def vehicleList() = game.trains
    // def updateInfo(selected: Option[Train]) = {}

}


sealed class ShipsPane(game: Game, w: VehiclesWindow)
  extends VehiclePane[Ship](game, "Ships", w) {

    def newWindow(game: Game, w: VehiclesWindow) = new NewShipWindow(game, w)
    def vehicleList() = game.ships
    // def updateInfo(selected: Option[Ship]) = {}

}









final class VehiclesWindow(
    private val game: Game)
  extends JFrame {

    val pane = new JTabbedPane();
    lazy val tabs = new Object {
      val trains = new TrainsPane(game, VehiclesWindow.this)
      val ships = new ShipsPane(game, VehiclesWindow.this)
    }

    setSize(480, 360)
    setTitle("Tchou:Vehicles")
    setVisible(false)
    makeLayout()

    protected def makeLayout() = {
      Seq(tabs.trains, tabs.ships)
        .foreach(tab => pane.addTab(tab.name, tab))
      add(pane)
    }


    // /* FIXME: add carriage view */
    //
    // private val trainsView = new GameListView[Train] {
    //     override def stringify(t: Train) = {
    //       val it = t.itinerary.map(_.city.name).mkString("-")
    //       f"<html><b>${t.id}</b> ($it)</html>"
    //     }
    // }
    //
    // private val statusUpdater = new Timer(100, new ActionListener() {
    //     def actionPerformed(e: ActionEvent) = VehiclesWindow.this.updateInfo()
    // })
    //
    // lazy val engineLabel = new JLabel("")
    // lazy val carriageLabel = new JLabel("")
    // lazy val passengersLabel = new JLabel("")
    // lazy val locationLabel = new JLabel("")
    //
    // lazy val newButton = new JButton("+")
    // lazy val rmButton = new JButton("-")
    //
    // //
    // setSize(480, 360)
    // setTitle("Tchou:Trains")
    // setVisible(false)
    // makeLayout()
    //
    // // Setup the displayed list
    // trainsView.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
    // fillTrainList()
    //
    // // Launch the thread in charge of updating the selected train status
    // statusUpdater.start()
    //
    // private def makeLayout() = {
    //     setLayout(new BorderLayout())
    //
    //     // Top bar label
    //     val status = new JPanel()
    //     status.setLayout(new GridLayout(1, 4))
    //     status.add(new JLabel("Trains: "))
    //     status.add(new JPanel())
    //
    //     newButton.addActionListener(this)
    //     status.add(newButton)
    //
    //     rmButton.addActionListener(this)
    //     status.add(rmButton)
    //
    //     add(status, BorderLayout.NORTH)
    //
    //     // Train list
    //     trainsView.setFixedCellWidth(420)
    //     trainsView.setFixedCellHeight(20)
    //     trainsView.setVisibleRowCount(-1)
    //
    //     val trainScroller = new JScrollPane(trainsView)
    //     trainScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS)
    //     add(trainScroller, BorderLayout.CENTER)
    //
    //     // Train info
    //     val info = Box.createHorizontalBox()
    //     val keys = Box.createVerticalBox()
    //     val values = Box.createVerticalBox()
    //
    //     keys.add(new JLabel("Engine: "))
    //     // keys.add(new JLabel("Carriage: "))
    //     // keys.add(new JLabel("Passengers: "))
    //     keys.add(new JLabel("Location: "))
    //
    //     values.add(engineLabel)
    //     // values.add(carriageLabel)
    //     // values.add(passengersLabel)
    //     values.add(locationLabel)
    //
    //     info.add(Box.createHorizontalStrut(5))
    //     info.add(keys)
    //     info.add(Box.createHorizontalStrut(10))
    //     info.add(values)
    //     add(info, BorderLayout.SOUTH)
    // }
    //
    // def fillTrainList() = {
    //     trainsView.setListData(game.trains)
    // }
    //
    // def updateInfo() = {
    //   trainsView.getSelectedItem() match {
    //       case Some(train) => {
    //           engineLabel.setText(f"${train.engine.name} (${train.engine.speed} km/h)")
    //           carriageLabel.setText(f"TODO")
    //           passengersLabel.setText(f"TODO")
    //           locationLabel.setText(train.location match {
    //               case station: Station => {
    //                   f"in ${station.city.name}"
    //               }
    //               case track: Track => {
    //                   val dst = train.roadmap.head
    //                   val src = track.getOpposite(dst)
    //                   f"between ${src.city.name} and ${dst.city.name} (${train.left} kms left)"
    //               }
    //           })
    //       }
    //       case None => {
    //           engineLabel.setText("")
    //           carriageLabel.setText("")
    //           passengersLabel.setText("")
    //           locationLabel.setText("")
    //       }
    //   }
    // }
    //
    // def actionPerformed(e: ActionEvent) {
    //     e.getSource() match {
    //         case `rmButton` => {
    //           trainsView.getSelectedItem().map(game.removeVehicle(_))
    //           fillTrainList()
    //         }
    //         case `newButton` => {
    //           new NewTrainWindow(game, TrainsWindow.this)
    //         }
    //         case _ => {}
    //     }
    // }
    //
    // // Unselect any selected train when the window is hidden / closed
    // override def setVisible(on: Boolean) = {
    //     trainsView.clearSelection()
    //     super.setVisible(on)
    // }

}
