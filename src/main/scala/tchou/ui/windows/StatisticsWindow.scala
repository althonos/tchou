package tchou.ui.windows

import java.awt.BorderLayout
import java.awt.BasicStroke
import java.awt.Color
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.JTabbedPane

import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartPanel
import org.jfree.chart.JFreeChart
import org.jfree.chart.axis.NumberAxis
import org.jfree.chart.plot.XYPlot
import org.jfree.chart.plot.PlotOrientation
import org.jfree.chart.renderer.xy.StackedXYBarRenderer
import org.jfree.data.xy.DefaultTableXYDataset;
import org.jfree.data.xy.XYDataset
import org.jfree.data.xy.XYSeries
import org.jfree.data.xy.XYSeriesCollection

import tchou.core.Statistics

class StatisticsWindow(
    val stats: Statistics)
  extends JFrame
{

    // Create a tabbed pane and an object storing all panels
    val pane = new JTabbedPane()
    val panels = new Object {
      val funds = fundsPanel()
      val soldResources = soldResourcesPanel()
      val earnedResources = earnedResourcesPanel()
    }

    // Add all tabs to the tabbed panes
    Map("Funds" -> panels.funds,
        "Resources (weight)" -> panels.soldResources,
        "Resources (money)" -> panels.earnedResources)
      .foreach(x => pane.addTab(x._1, x._2))

    // Add the tabbed pane to the window
    add(pane)

    // Finish initialising the statistics window.
    setSize(640, 480)
    setTitle("Tchou:Statistics")
    setLocationRelativeTo(null)
    setVisible(true)

    /** Create the panel displaying the funds over time.
     */
    private def fundsPanel(): JPanel = {
        val dataset = new XYSeriesCollection()
        dataset.addSeries(stats.funds)
        val chart = ChartFactory.createXYLineChart("Company Funds", "hours", "€", dataset)
        new ChartPanel(chart)
    }

    /** Create the panel displaying the total sold resources
     */
    private def soldResourcesPanel(): JPanel = {
        val dataset = new DefaultTableXYDataset()
        stats.soldResources.values.foreach(s => dataset.addSeries(s))
        val renderer = new StackedXYBarRenderer(0.10);
        renderer.setDrawBarOutline(false)
        val plot = new XYPlot(dataset, new NumberAxis("hours"), new NumberAxis("tons"), renderer)
        val chart = new JFreeChart("Sold Resources", plot)
        new ChartPanel(chart)
    }

    /** Create the panel displaying the total earned money by resources
     */
    private def earnedResourcesPanel(): JPanel = {
        val dataset = new DefaultTableXYDataset()
        stats.earnedResources.values.foreach(s => dataset.addSeries(s))
        val renderer = new StackedXYBarRenderer(0.10);
        renderer.setDrawBarOutline(false)
        val plot = new XYPlot(dataset, new NumberAxis("hours"), new NumberAxis("€"), renderer)
        val chart = new JFreeChart("Sold Resources", plot)
        new ChartPanel(chart)
    }

}
