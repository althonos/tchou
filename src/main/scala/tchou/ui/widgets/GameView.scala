package tchou.ui.widgets

import java.awt.BorderLayout
import java.awt.Color;
import java.awt.Component
import java.awt.Dimension
import java.awt.event.ActionListener
import java.awt.event.ActionEvent
import java.awt.event.MouseEvent
import java.awt.event.MouseListener

import javax.swing.Timer
import javax.swing.JPanel
import javax.swing.JTabbedPane

import scala.collection.JavaConverters._

import org.graphstream.graph.Graph
import org.graphstream.graph.Node
import org.graphstream.ui.swingViewer.ViewPanel
import org.graphstream.ui.view.View
import org.graphstream.ui.view.Viewer
import org.graphstream.ui.view.ViewerListener
import org.graphstream.ui.layout.springbox.implementations.SpringBox

import tchou.core.Game
import tchou.core.vehicles.Vehicle
import tchou.core.network.Network
import tchou.ui.windows.GameWindow




abstract class NetworkView(
    gameview: GameView)
  extends JPanel
  with ActionListener
{

    def network(game: Game): Network
    def vehicles(game: Game): Seq[Vehicle]

    var stylesheet = String.format("url(%s)", getClass().getResource("stylesheets/graph.css"))
    val graph = network(gameview.game).graph
    val viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD)
    val view: ViewPanel = viewer.addDefaultView(false)
    val vpipe = viewer.newViewerPipe()

    private val statusUpdater = new Timer(100, this)
    statusUpdater.start()

    graph.addAttribute("ui.quality")
    graph.addAttribute("ui.antialias")
    graph.addAttribute("ui.stylesheet", stylesheet)
    graph.addAttribute("ui.frozen")
    graph.addAttribute("layout.frozen")

    // Create the view and make it monitor clicks
    view.addMouseListener(gameview)
    view.setSize(1280, 720)
    vpipe.addViewerListener(gameview)

    //
    setSize(1280, 720)
    add(view)

    def updateInfo(game: Game) = {
      vehicles(game).foreach(v => {
        // Add node if it does not exits
        var node: Node = graph.getNode(v.id)
        if (node == null) {
          node = graph.addNode(v.id)
          node.setAttribute("ui.class", "vehicle")
        }
        // Set coordinates
        v.coordinates() match {
          case Some(c) => {
            node.setAttribute("xy", c.getLng(): java.lang.Double, c.getLat(): java.lang.Double)
          }
          case None => {}
        }
      })

      graph
        .getEachNode()
        .asScala
        .filter((n: Node) => n.getAttribute("ui.class") == "vehicle")
        .map((n: Node) => n.getId(): String)
        .filter(id => !vehicles(game).exists(_.id == id))
        .foreach(id => {
          game.logger.debug(f"Cleaning $id")
          val n: Node = graph.removeNode(id)
        })
    }

    def actionPerformed(e: ActionEvent) = {
      updateInfo(gameview.game)
    }

}



class GameView(private val window: GameWindow)
  extends JPanel
  with MouseListener
  with ViewerListener
{

    val game = window.game

    val pane = new JTabbedPane()

    val views = new Object {
      val rail = new NetworkView(GameView.this) {
        def network(game: Game) = game.networks.rail
        def vehicles(game: Game) = game.trains
      }
      val water = new NetworkView(GameView.this) {
        def network(game: Game) = game.networks.water
        def vehicles(game: Game) = game.ships
      }
    }

    //
    setLayout(new BorderLayout())
    setOpaque(true)
    setBackground(Color.WHITE)

    // Add the view to self and make it visible
    Map("Rail" -> views.rail, "Water" -> views.water)
      .foreach(x => pane.addTab(x._1, x._2.view))
    add(pane)
    setVisible(true);

    // Refresh the window
    repaint();
    revalidate();

    def showCityNames(on: Boolean) = {
        val mode = if (on) "normal" else "hidden"
        Seq(views.rail, views.water)
          .map(_.graph)
          .foreach(_.addAttribute("ui.stylesheet", s"node.city { text-mode: $mode ; }"))
    }

    def showTrackLengths(on: Boolean) = {
        val mode = if (on) "normal" else "hidden"
        Seq(views.rail, views.water)
          .map(_.graph)
          .foreach(_.addAttribute("ui.stylesheet", s"edge { text-mode: $mode ; }"))
    }

    def buttonPushed(name: String) = {
      window.citiesWindow.setVisible(true)
      window.citiesWindow.setSelectedCity(name)
    }

    def buttonReleased(name: String) = {}

    def viewClosed(id: String) = {}

    // Mouse Listener
    def mouseReleased(e: MouseEvent) = {
      Seq(views.rail, views.water)
        .foreach(_.vpipe.pump())
    }
    def mouseExited(e: MouseEvent) = {}
    def mouseEntered(e: MouseEvent) = {}
    def mousePressed(e: MouseEvent) = {}
    def mouseClicked(e: MouseEvent) = {}
}
