package tchou.ui.widgets

import java.awt.Color
import javax.swing.JLabel
import javax.swing.SwingUtilities
import scala.collection.mutable.ListBuffer


/** A JLabel mimicking the format of a console (white monospace text on a
 *  black background).
 */
class GameConsole
  extends JLabel
{

  val history: ListBuffer[String] = new ListBuffer()

  super.setOpaque(true)
  super.setBackground(Color.BLACK)
  info("")

  /** Toggle the visibility of the console.
   */
  def toggleVisible() {
    super.setVisible(!super.isVisible())
  }

  /** Show some text in the GameConsole.
   *
   *  @param text the text to show.
   */
  def info(text: String) {
    history += text
    SwingUtilities.invokeLater(new Runnable() {
      def run() {
        GameConsole.this.setText(
          "<html><font face=\"monospace\""
          +" color=\"white\"> &gt; "
          +text+"</font></html>"
        );
      }
    });
  }

}
