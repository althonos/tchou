package tchou.ui.widgets

import scala.xml.XML

import java.awt.event.ActionListener
import java.awt.event.ActionEvent
import java.awt.event.ItemEvent
import java.awt.event.ItemListener
import java.awt.event.WindowEvent
import javax.swing.JCheckBoxMenuItem
import javax.swing.JMenu
import javax.swing.JMenuBar
import javax.swing.JMenuItem
import javax.swing.JFileChooser
import javax.swing.JOptionPane

import tchou.core.Game
import tchou.ui.windows.GameWindow

class GameMenuBar(
    private val window: GameWindow)
  extends JMenuBar
  with ActionListener
{

    // File menu
    private val file_menu = new JMenu("File")
    add(file_menu)

    /// Button to save
    private val file_chooser = new SaveChooser(window)
    private val save_item = new JMenuItem("Save...")
    save_item.addActionListener(this)
    file_menu.add(save_item)

    /// Button to save
    private val load_item = new JMenuItem("Load...")
    load_item.addActionListener(this)
    file_menu.add(load_item)

    /// Button to quit
    private val quit_item = new JMenuItem("Quit")
    quit_item.addActionListener(this)
    file_menu.add(quit_item)

    // View menu
    private val view_menu = new JMenu("View")
    add(view_menu)

    /// Checkbox to toggle toolbar visibility
    private val show_toolbar_item = new JCheckBoxMenuItem("Show toolbar", true)
    show_toolbar_item.addActionListener(this)
    view_menu.add(show_toolbar_item)

    /// Checkbox to toggle console visibility
    // private val show_console_item = new JCheckBoxMenuItem("Show console", false)
    // show_console_item.addActionListener( new ActionListener {
    //     def actionPerformed(e: ActionEvent) = window.console.setVisible(show_console_item.getState())
    // })
    // view_menu.add(show_console_item)

    def actionPerformed(e: ActionEvent) = {
      e.getSource() match {
        // The `Save...` button was pressed: open a save dialog
        case `save_item` => {
          window.game.pause()
          val retval = file_chooser.showSaveDialog(window)
          if (retval == JFileChooser.APPROVE_OPTION && file_chooser.getSelectedFile() != null) {
            var path = file_chooser.getSelectedFile().getPath()
            XML.save(path, window.game.writeXml(), "UTF-8", true, null)
            JOptionPane.showMessageDialog(window, "Saved game successfully !");
          }
          window.game.unpause()
        }
        // The `Load...` button was pressed: open a load dialog
        case `load_item` => {
          window.game.pause()
          val retval = file_chooser.showOpenDialog(window)
          if (retval == JFileChooser.APPROVE_OPTION && file_chooser.getSelectedFile() != null) {
            val xml = XML.loadFile(file_chooser.getSelectedFile().getPath())
            val game = Game.readXml(xml)
            game.pause();
            window.game = game
            JOptionPane.showMessageDialog(window, "Loaded game successfully !");
          }
          window.game.unpause()
        }
        // The `Show Toolbar` checkbox was toggled: update
        case `show_toolbar_item` => {
          window.toolbar.setVisible(show_toolbar_item.getState())
        }
        // The `Quit` button was pressed: quit
        case `quit_item` => {
          window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
        }
      }
    }

}
