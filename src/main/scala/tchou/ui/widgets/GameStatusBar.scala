package tchou.ui.widgets

import tchou.core.Game

import java.awt.Component
import java.awt.Dimension
import java.awt.event.ActionListener
import java.awt.event.ActionEvent

import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.BoxLayout
import javax.swing.Box
import javax.swing.Timer

class GameStatusBar(
    private val game: Game)
  extends JPanel
  with ActionListener
{

    lazy val timer = new Timer(500, this)
    lazy val money_label = new JLabel(game.money.toString())
    lazy val time_label = new JLabel(game.time.toString())

    setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS))
    add(Box.createRigidArea(new Dimension(74,0)))
    add(new JLabel("Money: "))
    add(Box.createRigidArea(new Dimension(0,5)))
    add(money_label)

    add(Box.createRigidArea(new Dimension(64, 0)))
    add(new JLabel("Time: "))
    add(Box.createRigidArea(new Dimension(0,5)))
    add(time_label)

    timer.start()

    def actionPerformed(e: ActionEvent) = {
        val hours = game.time % 24
        val day = (game.time / 24) % 365 + 1
        val year = ((game.time / 24) / 365) + 1
        money_label.setText(game.money.toString())
        time_label.setText(f"Year $year, Day $day, $hours%02d:00")
    }

}
