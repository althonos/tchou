package tchou.ui.widgets

import javax.swing.JFileChooser
import javax.swing.filechooser.FileFilter

import tchou.ui.windows.GameWindow


class XmlFilter
  extends FileFilter
{
    def getDescription(): String = "XML saved games"
    def accept(file: java.io.File): Boolean = {
      file.isDirectory() || file.getPath().toLowerCase().endsWith(".xml")
    }
}

class SaveChooser(
    private val window: GameWindow)
  extends JFileChooser
{
   	setFileFilter(new XmlFilter())
}
