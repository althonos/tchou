package tchou.ui.widgets

import javax.swing.JList

import scala.collection.JavaConverters._


class GameListView[T]
  extends JList[String]
{

    private var seq: Option[Seq[T]] = None

    def setListData(seq: Seq[T]) = {
        val items = seq.map(stringify)
        this.seq = Some(seq)
        super.setListData(new java.util.Vector(items.asJava))
    }

    def stringify(that: T): String = {
        that.toString()
    }

    def getSelectedItem(): Option[T] = {
        this.getSelectedIndex() match {
            case -1 => None;
            case idx => Some(seq.get(idx));
        }
    }

    def indexOf(elem: T): Integer = {
        seq.map(_.indexOf(elem): Integer).getOrElse(-1)
    }

}
