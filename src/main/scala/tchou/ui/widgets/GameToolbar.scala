package tchou.ui.widgets

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon
import javax.swing.JButton
import javax.swing.JToggleButton
import javax.swing.JToolBar
import javax.swing.SwingConstants

import tchou.ui.windows.CitiesWindow
import tchou.ui.windows.GameWindow
import tchou.ui.windows.StatisticsWindow
import tchou.ui.windows.VehiclesWindow


class GameToolbar(
    val window: GameWindow,
    val orientation: Int = SwingConstants.VERTICAL)
  extends JToolBar(orientation)
  with ActionListener
{

    // Button to show trains information
    private val trains_button = new JButton(new ImageIcon(getClass().getResource("icons/trains.png")))
    trains_button.addActionListener(this)
    trains_button.setToolTipText("Trains");
    add(trains_button)

    // Button to show ships information
    private val ships_button = new JButton(new ImageIcon(getClass().getResource("icons/ships.png")))
    ships_button.addActionListener(this)
    ships_button.setToolTipText("Ships");
    add(ships_button)

    // Button to show cities information
    private val city_button = new JButton(new ImageIcon(getClass().getResource("icons/city.png")))
    city_button.addActionListener(this)
    city_button.setToolTipText("Cities");
    add(city_button)

    /* TODO: icon - Toggle button to show stats*/
    private val stats_button = new JButton(new ImageIcon(getClass().getResource("icons/stats.png")))
    stats_button.addActionListener(this)
    stats_button.setToolTipText("Statistics")
    add(stats_button)

    /**********************************************************************************************/
    add(new JToolBar.Separator )
    /**********************************************************************************************/

    // Toggle button to display or hide city names
    private val names_button = new JToggleButton("Names", true)
    names_button.addActionListener(this)
    names_button.setToolTipText("Show City Names")
    add(names_button)

    // Toggle button to display or hide track lengths
    private val lengths_button = new JToggleButton("Lengths", true)
    lengths_button.addActionListener(this)
    lengths_button.setToolTipText("Show Tranck Lengths")
    add(lengths_button)

    def actionPerformed(e: ActionEvent) = {
      e.getSource() match {
        case `trains_button` => {
          window.vehiclesWindow.setVisible(true)
          window.vehiclesWindow.pane.setSelectedComponent(window.vehiclesWindow.tabs.trains)
        }
        case `ships_button` => {
          window.vehiclesWindow.setVisible(true)
          window.vehiclesWindow.pane.setSelectedComponent(window.vehiclesWindow.tabs.ships)
        }
        case `city_button` => window.citiesWindow.setVisible(true)
        case `stats_button` => new StatisticsWindow(window.game.stats)
        case `names_button` => window.view.showCityNames(names_button.isSelected())
        case `lengths_button` => window.view.showTrackLengths(lengths_button.isSelected())
        case _ => ()
      }
    }

}
