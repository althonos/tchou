package tchou.ui.widgets

import java.awt.BorderLayout
import java.awt.GridLayout
import java.awt.Dimension

import javax.swing.Box
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.JLabel
import javax.swing.JComboBox
import javax.swing.JButton
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.BoxLayout
import javax.swing.JScrollPane
import javax.swing.ScrollPaneConstants
import javax.swing.ListSelectionModel
import javax.swing.event.ListSelectionListener
import javax.swing.event.ListSelectionEvent

import scala.collection.immutable.Seq
import scala.collection.mutable.Buffer
import scala.collection.mutable.ArrayBuffer

import tchou.core.eco.City
import tchou.core.Game
import tchou.core.network.Station
import tchou.core.network.Network
import tchou.core.vehicles.trains.Train
import tchou.core.vehicles.trains.Engine
import tchou.core.vehicles.trains.Carriage


abstract class ItinerarySelector(game: Game)
  extends Box(BoxLayout.X_AXIS)
  with ActionListener
{

    def network(game: Game): Network

    private val _selected: Buffer[Station] = new ArrayBuffer()

    private val selectedStations = new GameListView[Station] {
      override def stringify(s: Station) = f"<html><b>${s.city.name}</b></html>"
    }

    private val availableStations = new GameListView[Station] {
      override def stringify(s: Station) = f"<html><b>${s.city.name}</b></html>"
    }

    lazy val addButton = new JButton("+")
    lazy val rmButton = new JButton("-")
    addButton.addActionListener(this)
    rmButton.addActionListener(this)

    makeLayout()
    fillAvailableList()
    setVisible(true)

    private def makeLayout() = {
      // val box = Box.createHorizontalBox()
      // box.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE))

      val selectedScroller = new JScrollPane(selectedStations)
      selectedScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS)
      selectedScroller.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE))
      // box.add(selectedScroller)
      add(selectedScroller)

      val buttons = Box.createVerticalBox()
      addButton.setMaximumSize(new Dimension(40, addButton.getMinimumSize().height));
      rmButton.setMaximumSize(new Dimension(40, rmButton.getMinimumSize().height));
      buttons.add(addButton)
      buttons.add(rmButton)
      // box.add(buttons)
      add(buttons)

      val availableScroller = new JScrollPane(availableStations)
      availableScroller.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE))
      availableScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS)
      // box.add(availableScroller)
      add(availableScroller)
    }

    private def fillAvailableList() = {
      val stations = if (_selected.isEmpty) {
        network(game).stations.map(_._2)
      } else {
        val last = _selected.last
        network(game).connectedStations(last).filter(_ != last)
      }
      availableStations.setListData(stations.toSeq.sortBy(_.city.name))
    }

    private def fillSelectedList() = {
      selectedStations.setListData(_selected)
    }

    def actionPerformed(e: ActionEvent) {
        e.getSource() match {
            case `rmButton` => {
              if (!_selected.isEmpty) {
                _selected.trimEnd(1)
              }
              fillAvailableList()
              fillSelectedList()
            }
            case `addButton` => {
              availableStations.getSelectedItem() match {
                case Some(c) => {_selected += c}
                case _ => {}
              }
              fillAvailableList()
              fillSelectedList()
            }
            case _ => {}
        }
    }

    def selected: Buffer[Station] = _selected

}
