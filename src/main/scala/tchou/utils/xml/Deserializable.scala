package tchou.utils.xml

import scala.xml.Node

import tchou.core.Game

/** Objects that can be deserialized from XML, but need to access Game data to do so.
 */
protected[tchou] abstract trait XmlGameDeserializable[T] {
  def readXml(xml: Node, game: Game): T
}

/** Objects that can be deserialized from XML.
 */
protected[tchou] abstract trait XmlDeserializable[T]
  extends XmlGameDeserializable[T]
{
  def readXml(xml: Node): T
  def readXml(xml: Node, game: Game): T = readXml(xml)
}
