package tchou.utils.xml

import scala.xml.Node

protected[tchou] abstract trait XmlSerializable {
  def writeXml(): Node
}
