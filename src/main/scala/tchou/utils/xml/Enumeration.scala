package tchou.utils.xml

import scala.collection.immutable.Map
import scala.xml.Node
import scala.xml.XML

import tchou.utils.Named

protected[tchou] abstract trait XmlEnum[T <: Named]
  extends XmlDeserializable[T] {

  lazy val instances: Map[String, T] = {
    val classname = getClass().getSimpleName().stripSuffix("$")
    val handle = getClass().getResourceAsStream(f"${classname.toLowerCase}s.xml")
    (XML.load(handle) \\ classname).map(readXml).map(x => x.name -> x).toMap
  }

}
