package tchou.utils

protected[tchou] trait Named {
    val name: String
}
