package tchou.core.vehicles.ships

import scala.xml.Node

import tchou.core.eco.cargo.Cargo
import tchou.utils.Named
import tchou.utils.xml.XmlEnum


sealed case class ShipClass(
    val name: String,
    val speed: Integer,
    val capacity: Integer,
    val accepts: Seq[Cargo])
  extends Named


object ShipClass extends XmlEnum[ShipClass] {
  def readXml(xml: Node): ShipClass =
    new ShipClass(
      (xml \ "@name").text,
      (xml \ "@speed").text.toInt,
      (xml \ "@capacity").text.toInt,
      (xml \\ "accept" \\ "@cargo").map(n => Cargo.instances(n.text)).toSeq
    )
}
