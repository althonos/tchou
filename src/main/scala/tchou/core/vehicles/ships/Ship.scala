package tchou.core.vehicles.ships

import scala.collection.immutable.Seq
import scala.xml.Node

import com.typesafe.scalalogging.Logger

import tchou.core.Game
import tchou.core.eco.cargo.Cargo
import tchou.core.network.Workshop
import tchou.core.network.Station
import tchou.core.network.ships.WaterNetwork
import tchou.core.vehicles.Vehicle
import tchou.utils.Haiku
import tchou.utils.xml.XmlSerializable
import tchou.utils.xml.XmlGameDeserializable

class Ship(
    val cls: ShipClass,
    val resource: Cargo,
    itinerary: Seq[Station],
    val id: String = Haiku.generate(),
    logger: Logger = Logger[Ship])
  extends Vehicle(itinerary, logger)
  with XmlSerializable
{

    override def toString: String = {
      f"Ship('${this.id}')"
    }

    /* FIXME */
    def cost() = {
      1
    }

    def capacity() = {
        Map(resource -> cls.capacity)
    }

    def speed() = {
        cls.speed
    }

    def network(game: Game): WaterNetwork = {
      game.networks.water
    }

    def writeXml(): Node = {
      <Ship id={id} class={cls.name} cargo={resource.name} index={_itineraryIndex.toString} left={_left.toString}>
        {_itinerary.map(s => <Station name={s.name}/>)}
        {_roadmap.map(s => <Step name={s.name}/>)}
        {_location.writeXml()}
        {_load.map(r => <Cargo name={r._1.name} amount={r._2.toString}/>)}
      </Ship>
    }

}


object Ship
  extends XmlGameDeserializable[Ship]
{
  def readXml(xml: Node, game: Game): Ship = {
    val id = (xml \ "@id").text
    val cls = ShipClass.instances((xml \ "@class").text)
    val cargo = Cargo.instances((xml \ "@cargo").text)
    val itinerary = (xml \ "Station").map(s => game.networks.water.stations((s \ "@name").text))
    val ship = new Ship(cls, cargo, itinerary, id)

    // Load the current position in the itinerary
    ship._itineraryIndex = (xml \ "@index").text.toInt

    // Load the number of kms left to destination
    ship._left = (xml \ "@left").text.toInt

    // Load the current location of the ship
    val loctype = (xml \ "Location" \ "@type").text
    ship._location = if (loctype == "station") {
      game.networks.water.stations( (xml \ "Location" \ "@name").text)
    } else if (loctype == "track") {
      val src = game.networks.water.stations((xml \ "Location" \ "@src").text)
      val dst = game.networks.water.stations((xml \ "Location" \ "@dst").text)
      game.networks.water.tracks(src)(dst)
    } else {
      Workshop
    }

    // Load the roadmap of the train, so it knows where to go next
    ship._roadmap ++= (xml \ "Step").map(s => game.networks.water.stations((s \ "@name").text))

    // Load the resources the ship is currently carrying
    (xml \ "Cargo").foreach(c => {
        ship._load.update(Cargo.instances((c \ "@name").text), (c \ "@amount").text.toInt)
    })

    ship
  }
}
