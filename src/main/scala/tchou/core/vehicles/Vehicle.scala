package tchou.core.vehicles

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap
import scala.collection.mutable.{Map => MutMap}
import scala.collection.mutable.Buffer
import scala.collection.immutable.Map
import scala.collection.immutable.Seq

import com.typesafe.scalalogging.Logger
import uk.me.jstott.jcoord.LatLng

import tchou.core.Game
import tchou.core.eco.cargo.Cargo
import tchou.core.network.Network
import tchou.core.network.Location
import tchou.core.network.Station
import tchou.core.network.Track
import tchou.core.network.Workshop
import tchou.core.vehicles.trains.Train
import tchou.utils.xml.XmlSerializable


abstract class Vehicle(
    it: Seq[Station],
    val logger: Logger = Logger[Vehicle])
  extends XmlSerializable
{

     ////////////////
    // ATTRIBUTES //
   ////////////////

    /** An ID for the vehicle.
     */
    val id: String

    /** The location of the vehicle.
     */
    protected var _location: Location = it.headOption.getOrElse(Workshop)

    /** The sequence of cities this vehicle must go to.
     *
     *  If this sequence starts and ends with the same city,
     *  the path will be considered as cyclic.
     */
    protected val _itinerary: Buffer[Station] = new ArrayBuffer()

    /** The position of the train in its itinerary.
     */
    protected var _itineraryIndex: Integer = 0

    /** The sequence of cities this vehicle will go to.
     */
    protected val _roadmap: Buffer[Station] = new ArrayBuffer()


    /** The distance left before reaching the destination (in km).
     */
    protected var _left: Integer = 0

    /** The amount of resource carried by the vehicle.
     */
    protected var _load: MutMap[Cargo, Integer] = new HashMap()

    // type TrackType <: Track

    // Build the itinerary from the given sequence
    if (!it.isEmpty) {
      if (it.head == it.last) {
        _itinerary ++= it.dropRight(1)
      } else {
        _itinerary ++= it
        _itinerary ++= it.drop(1).reverse.drop(1)
      }
    }

     //////////////////////
    // ABSTRACT METHODS //
   //////////////////////

    /** The maximum cargo this vehicle can carry.
     */
    def capacity(): Map[Cargo, Integer]

    /** The speed of the vehicle, in km/h.
     */
    def speed(): Integer

    /** The money it costs to operate the vehicle every tick.
     */
    def cost(): Integer

    /**
     */
    def network(game: Game): Network

      //////////////////////
     // CONCRETE METHODS //
    //////////////////////

    def roadmap: Seq[Station] = _roadmap.to[Seq]
    def itinerary: Seq[Station] = _itinerary.to[Seq]
    def location: Location = _location
    def left: Integer = _left
    def load: Map[Cargo, Integer] = _load.toMap

    def coordinates(): Option[LatLng] = {
      _location match {
        case station: Station => Some(station.city.coordinates)
        case track: Track => {
          val dst = _roadmap.head.city.coordinates
          val src = track.getOpposite(_roadmap.head).city.coordinates
          val lat = (dst.getLat() * (track.length() - _left) + src.getLat() * (_left)) / track.length()
          val lng = (dst.getLng() * (track.length() - _left) + src.getLng() * (_left)) / track.length()
          Some(new LatLng(lat, lng))
        }
        case _ => None
      }
    }

    final private def loadCargo(game: Game, station: Station) = {
      // Resources the city accepts
      val produces = station.city.produces
      //
      for ((cargo: Cargo, maxLoad: Integer) <- capacity()) {
        if (produces.contains(cargo)) {
          val loadable = Math.min(maxLoad - _load.getOrElse(cargo, new Integer(0)), station.city.resources(cargo))
          station.city.buyResource(game, cargo, loadable)
          _load(cargo) = _load.getOrElse(cargo, new Integer(0)) + loadable
        }
      }
    }

    final private def unloadCargo(game: Game, station: Station) = {
      //
      val wants = station.city.wants
      for ((cargo: Cargo, load: Integer) <- _load) {
        if (wants.contains(cargo)) {
          station.city.sellResource(game, cargo, load)
          _load(cargo) = new Integer(0)
        }
      }
    }

    /* FIXME */
    final def update(game: Game): Unit = {
      _location match {
        case station: Station => update(game, station)
        case track: Track => update(game, track)
        case _ => ()
      }
      game.money -= cost()
    }

    /** Update the vehicle if it is in a city.
     */
    final private def update(game: Game, station: Station): Unit = {
      logger.debug(f"$this is currently in ${station.city.name}")

      // Get the right network the vehicle is running on
      val network = this.network(game)

      // This is not just a travel step
      if (_itinerary.contains(station)) {
          logger.debug(f"$this is unloading/loading cargo")
          // logger.debug(f"Before unload: $load")
          unloadCargo(game, station)
          // logger.debug(f"Before load: $load")
          loadCargo(game, station)
          // logger.debug(f"After load: $load")
      }

      //
      if (_roadmap.isEmpty) {

          val next = if (_itinerary.isDefinedAt(_itineraryIndex+1)) {
            _itineraryIndex += 1
            _itinerary(_itineraryIndex)
          } else {
            _itineraryIndex = 0
            _itinerary.head
          }

          _roadmap ++= network.shortestPath(station, next)
          _roadmap.remove(0)

          logger.debug(f"$this is next going to ${next.name} (${_roadmap.map(_.name)})")
      }

      val track = network.tracks(station)(_roadmap.head)
      _location = track
      _left = track.length().floor.toInt
    }

    /** Update the vehicle if it is on a track.
     */
    final private def update(game: Game, track: Track): Unit = {
      logger.debug(f"$this is currently going to ${_roadmap.head.name} (${_left}km left, ${speed()} km/h})")
      _left -= speed()

      if (_left < 0) {
          _left = 0
          _location = _roadmap.head
          _roadmap.remove(0)
      }
    }


}
