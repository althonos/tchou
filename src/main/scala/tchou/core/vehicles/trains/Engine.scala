package tchou.core.vehicles.trains

import scala.xml.Node

import tchou.utils.Named
import tchou.utils.xml.XmlEnum

sealed case class Engine (
    val name: String,
    val speed: Integer,
    val kerb: Integer,
    val power: Integer,
    val price: Integer,
    val cost: Integer)
  extends Named

object Engine extends XmlEnum[Engine] {
  def readXml(xml: Node): Engine = {
    val name = (xml \\ "@name").text
    val speed = (xml \\ "@speed").text.toInt
    val weight = (xml \\ "@kerb").text.toInt
    val power = (xml \\ "@power").text.toInt
    val price = (xml \\ "@price").text.toInt
    val cost = (xml \\ "@cost").text.toInt
    new Engine(name, speed, weight, power, price, cost)
  }
}
