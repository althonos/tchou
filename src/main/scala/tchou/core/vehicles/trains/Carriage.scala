package tchou.core.vehicles.trains

import scala.xml.Node

import tchou.core.eco.cargo.Cargo
import tchou.utils.Named
import tchou.utils.xml.XmlEnum

sealed case class Carriage(
    val name: String,
    val cargo: Cargo,
    val capacity: Integer,
    val kerb: Integer)
  extends Named

object Carriage extends XmlEnum[Carriage] {
  def readXml(xml: Node): Carriage =
    new Carriage(
      (xml \ "@name").text,
      Cargo.instances((xml \ "@cargo").text),
      (xml \ "@capacity").text.toInt,
      (xml \ "@kerb").text.toInt
    )
}
