package tchou.core.vehicles.trains

import scala.collection.immutable.Map
import scala.collection.immutable.List
import scala.collection.immutable.Seq
import scala.collection.mutable.HashMap
import scala.collection.mutable.Buffer
import scala.collection.mutable.ListBuffer
import scala.xml.Node

import com.typesafe.scalalogging.Logger

import tchou.core.Game
import tchou.core.eco.cargo.Cargo
import tchou.core.network.Track
import tchou.core.network.Station
import tchou.core.network.Workshop
import tchou.core.network.trains.RailNetwork
import tchou.core.vehicles.Vehicle
import tchou.utils.Haiku
import tchou.utils.xml.XmlSerializable
import tchou.utils.xml.XmlGameDeserializable

class Train(
    val engine: Engine,
    cars: Seq[Carriage],
    itinerary: Seq[Station],
    val id: String = Haiku.generate(),
    logger: Logger = Logger[Train])
  extends Vehicle(itinerary, logger)
  with XmlSerializable
{

    def this(engine: Engine, itinerary: Seq[Station]) = {
        this(engine, Seq.empty, itinerary)
    }

    private var _carriages: Buffer[Carriage] = new ListBuffer()
    _carriages ++= cars

    def carriages: Seq[Carriage] = _carriages.to[Seq]

    override def toString: String = {
      f"Train('${this.id}')"
    }

    /* FIXME */
    def cost() = {
      val cost: Double = engine.cost.toDouble * (1.0 + _carriages.length.toDouble) / 50.0
      cost.ceil.toInt
    }

    def capacity() = {
        carriages
          .groupBy(_.cargo)
          .mapValues(_.foldLeft(0)(_ + _.capacity))
    }

    /* FIXME */
    def speed() = {

        // The individual weights of the train
        val kerb: Integer = carriages.foldLeft(0)(_ + _.kerb) + engine.kerb
        val net: Integer = load.foldLeft(0)(_ + _._2)

        // The total weight the engine is hauling
        val weight = kerb + net

        //
        val speed = engine.speed * scala.math.log10(engine.power * 1.0) / scala.math.log10(weight)

        // Make sure the speed is never zero, this could be problematic.
        speed.ceil.toInt
    }

    def network(game: Game): RailNetwork = {
      game.networks.rail
    }

    def writeXml(): Node = {
      <Train id={id} engine={engine.name} index={_itineraryIndex.toString} left={_left.toString}>
        {_carriages.map(c => <Carriage name={c.name}/>)}
        {_itinerary.map(s => <Station name={s.name}/>)}
        {_roadmap.map(s => <Step name={s.name}/>)}
        {_location.writeXml()}
        {_load.map(r => <Cargo name={r._1.name} amount={r._2.toString}/>)}
      </Train>
    }

}


object Train
  extends XmlGameDeserializable[Train]
{
  def readXml(xml: Node, game: Game): Train = {
    val id = (xml \ "@id").text
    val engine = Engine.instances((xml \ "@engine").text)
    val carriages = (xml \ "Carriage").map(x => Carriage.instances((x \ "@name").text))
    val itinerary = (xml \ "Station").map(s => game.networks.rail.stations((s \ "@name").text))
    val train = new Train(engine, carriages, itinerary, id)

    // Load the current position in the itinerary
    train._itineraryIndex = (xml \ "@index").text.toInt

    // Load the number of kms left to destination
    train._left = (xml \ "@left").text.toInt

    // Load the current location of the train
    val loctype = (xml \ "Location" \ "@type").text
    train._location = if (loctype == "station") {
        game.networks.rail.stations( (xml \ "Location" \ "@name").text)
    } else if (loctype == "track") {
        val src = game.networks.rail.stations((xml \ "Location" \ "@src").text)
        val dst = game.networks.rail.stations((xml \ "Location" \ "@dst").text)
        game.networks.rail.tracks(src)(dst)
    } else {
      Workshop
    }

    // Load the roadmap of the train, so it knows where to go next
    train._roadmap ++= (xml \ "Step").map(s => game.networks.rail.stations((s \ "@name").text))

    // Load the resources the train is currently carrying
    (xml \ "Cargo").foreach(c => {
        train._load.update(Cargo.instances((c \ "@name").text), (c \ "@amount").text.toInt)
    })

    train

  }
}
