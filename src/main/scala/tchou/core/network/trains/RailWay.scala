package tchou.core.network.trains


import com.typesafe.scalalogging.Logger
import org.graphstream.graph.Edge

import tchou.core.network.Track

class RailWay(
    src: TrainStation,
    dst: TrainStation,
    edge: Edge)
  extends Track(src, dst, edge) {

}
