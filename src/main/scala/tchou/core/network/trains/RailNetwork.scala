package tchou.core.network.trains

import com.typesafe.scalalogging.Logger

import tchou.core.network.Network

class RailNetwork(
    name: String,
    logger: Logger = Logger[RailNetwork])
  extends Network(name, logger) {
}
