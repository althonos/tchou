package tchou.core.network.trains

import org.graphstream.graph.Node

import tchou.core.eco.City
import tchou.core.network.Station


class TrainStation(
    city: City,
    node: Node)
  extends Station(city, node) {

}
