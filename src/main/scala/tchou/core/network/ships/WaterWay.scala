package tchou.core.network.ships

import com.typesafe.scalalogging.Logger
import org.graphstream.graph.Edge

import tchou.core.network.Track

class WaterWay(
    src: Port,
    dst: Port,
    edge: Edge)
  extends Track(src, dst, edge) {

}
