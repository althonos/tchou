package tchou.core.network.ships

import com.typesafe.scalalogging.Logger
import org.graphstream.graph.Node
import org.graphstream.graph.Edge

import tchou.core.eco.City
import tchou.core.network.Network

class WaterNetwork(
    name: String,
    logger: Logger = Logger[WaterNetwork])
  extends Network(name, logger) {

    def createStation(city: City, node: Node): Port = {
        new Port(city, node)
    }

    def createTrack(src: Port, dst: Port, edge: Edge) = {
        new WaterWay(src, dst, edge)
    }

}
