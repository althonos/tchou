package tchou.core.network.ships

import org.graphstream.graph.Node

import tchou.core.eco.City
import tchou.core.network.Station


class Port(
    city: City,
    node: Node)
  extends Station(city, node) {

}
