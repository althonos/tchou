package tchou.core.network

import scala.xml.Node

import org.graphstream.graph.Edge

import tchou.utils.xml.XmlSerializable



class Track(
    val src: Station,
    val dst: Station,
    val edge: Edge)
  extends Location {

    edge.setAttribute("length", length(): java.lang.Double)
    edge.setAttribute("label", length().floor.toInt: java.lang.Integer)

    /** Given a city, return the city at the other end of the Track.
     *
     *  @param city One of the track endpoints.
     *  @return The other track endpoint.
     */
    def getOpposite(station: Station): Station = {
        station match {
            case `src` => dst;
            case `dst` => src;
        }
    }

    /** Return the length of the track.
     *
     *  Calculated using the GPS coordinates of each endpoint.
     *
     *  @return The distance between the track endpoints, in kilometers.
     */
    def length(): Double = {
        src.city.distance(dst.city)
    }

    /** Serialize this track to an XML location node
     */
    def writeXml() = {
      <Location type="track" src={src.name} dst={dst.name}/>
    }

}
