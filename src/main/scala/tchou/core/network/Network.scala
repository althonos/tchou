package tchou.core.network

import scala.collection.immutable.Map
import scala.collection.mutable.HashMap
import scala.collection.mutable.{Map => MutableMap}
import scala.collection.JavaConverters._
import scala.xml.{Node => XmlNode}

import com.typesafe.scalalogging.Logger
import org.graphstream.algorithm.Dijkstra
import org.graphstream.graph.Graph
import org.graphstream.graph.Edge
import org.graphstream.graph.Node
import org.graphstream.graph.implementations.SingleGraph
import uk.me.jstott.jcoord.LatLng

import tchou.core.eco.City
import tchou.core.vehicles.Vehicle
import tchou.utils.xml.XmlDeserializable


abstract class Network(
    name: String,
    val logger: Logger = Logger[Network])
  {

    /** The wrapped graph instance used to compute shortest paths.
     */
    val graph: Graph = new SingleGraph(name)

    /** A collection of the network stations, referenced by their city names.
     */
    protected val _stations: MutableMap[String, Station] = new HashMap()

    /** A collection to the network tracks, referenced by their endpoints.
     */
    protected val _tracks: MutableMap[Station, MutableMap[Station, Track]] = new HashMap()

    /** A read-only immutable view of the stations of the network.
     */
    def stations: Map[String, Station] = _stations.toMap

    /** A read-only immutable view of the tracks of the network.
     */
    def tracks: Map[Station, Map[Station, Track]] = _tracks.mapValues(_.toMap).toMap

    /** An iterable over the unique tracks of the network.
     */
    def trackStream: Stream[Track] = _tracks.values.toStream.flatMap(_.values.toStream).distinct

    /** Register a city within the network internal data structures.
     */
    protected def registerStation(station: Station): Station = {
      _stations.update(station.city.name, station)
      _tracks.update(station, new HashMap())
      station
    }

    /** Register a tack within the network internal data structures.
     */
    protected def registerTrack(track: Track): Track = {
        _tracks(track.src).update(track.dst, track)
        _tracks(track.dst).update(track.src, track)
        track
    }

    /** Add a city.
     *
     *  @param name The name of the new city.
     *  @param size The size of the new city.
     *  @param coordinates The position of the new city.
     *  @return A new City instance.
     */
    def addStation(city: City): Station = {
        val node: Node = graph.addNode(f"${city.name}")
        registerStation(new Station(city, node))
    }

    /** Add a track with the given endpoints.
     *
     *  The order of the parameters is not important as the graph
     *  is not directed.
     *
     *  @param src The City from which the track is leaving.
     *  @param dst The City the track is going to.
     *  @return A new Track instance.
     */
    def addTrack(src: Station, dst: Station): Track = {
        val edge: Edge = graph.addEdge(f"${src.city.name}:${dst.city.name}", src.node, dst.node, false)
        registerTrack(new Track(src, dst, edge))
    }

    /** Add a track with the given endpoints, using city names.
     *
     *  The order of the parameters is not important as the graph
     *  is not directed.
     *
     *  @param src The City from which the track is leaving.
     *  @param dst The City the track is going to.
     *  @return A new Track instance.
     */
    def addTrack(src: String, dst: String): Track = {
        addTrack(stations(src), stations(dst))
    }

    /** Get the shortest path between two stations.
     */
    def shortestPath(src: Station, dst: Station): Seq[Station] = {
      var dijkstra = new Dijkstra(Dijkstra.Element.EDGE, "dijkstra", "length")
      dijkstra.init(graph)
      dijkstra.setSource(src.node)
      dijkstra.compute()

      dijkstra
        .getPath(dst.node)
        .getNodePath()
        .asScala
        .map(_.getId())
        .map(_stations)
    }

    /** Get a seq of stations connected to the given station.
     *
     *  Note:
     *      the station passed in argument is included in the
     *      returned sequence.
     */
    def connectedStations(station: Station): Seq[Station] = {
      var dijkstra = new Dijkstra(Dijkstra.Element.EDGE, "dijkstra", "length")
      dijkstra.init(graph)
      dijkstra.setSource(station.node)
      dijkstra.compute()

      graph
        .getEachNode()
        .asScala
        .filter((node: Node) => dijkstra.getPathLength(node) < java.lang.Double.POSITIVE_INFINITY)
        .map((node: Node) => node.getId())
        .map(_stations)
        .toSeq
    }

}
