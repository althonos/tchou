package tchou.core.network

import org.graphstream.graph.Node

import tchou.core.eco.City



class Station(
    val city: City,
    val node: Node)
  extends Location {

    val name = city.name
    node.setAttribute("label", city.name)
    node.setAttribute("xy", city.longitude: java.lang.Double, city.latitude: java.lang.Double)
    node.setAttribute("ui.class", "city")

    def writeXml() = {
      <Location type="station" name={name}/>
    }

}
