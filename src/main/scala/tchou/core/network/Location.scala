package tchou.core.network

import tchou.utils.xml.XmlSerializable

/** A Location on the game world.
 */
abstract class Location extends XmlSerializable

/**
 */
object Workshop extends Location {
  def writeXml() = <Location type="workshop"/>
}
