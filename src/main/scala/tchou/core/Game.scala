package tchou.core

import java.util.UUID
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Buffer
import scala.collection.mutable.{Map => MutMap}
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.xml.Node

import com.typesafe.scalalogging.Logger
import uk.me.jstott.jcoord.LatLng

import tchou.core.eco.City
import tchou.core.eco.cargo.Cargo
import tchou.core.network.Network
import tchou.core.network.Track
import tchou.core.network.ships.WaterNetwork
import tchou.core.network.trains.RailNetwork
import tchou.core.vehicles.Vehicle
import tchou.core.vehicles.ships.Ship
import tchou.core.vehicles.ships.ShipClass
import tchou.core.vehicles.trains.Train
import tchou.core.vehicles.trains.Engine
import tchou.core.vehicles.trains.Carriage
import tchou.utils.xml.XmlDeserializable
import tchou.utils.xml.XmlSerializable


class Game(
    val logger: Logger = Logger[Game])
  extends XmlSerializable
  {

    /** Create a `Game` with a default logger.
     */
    def this() = {
      this(Logger[Game])
    }

    /** The game unique identifier.
     */
    private lazy val id = UUID.randomUUID().toString()

    /** The ingame time (hours since start)
     */
    private var _time: Integer = 0

    /** Count the number of individual pause requests.
     */
    private var _pauseHandler: Integer = 0

    /** The player funds.
     */
    var money: Integer = 0

    /** The ingame transportation networks.
     */
    val networks = new Object {
      val rail: RailNetwork = new RailNetwork(id, logger)
      val water: WaterNetwork = new WaterNetwork(id, logger)
    }

    /** Some statistics about the game
     */
    val stats = new Statistics()

    /** A collection of ingame trains.
     *
     *  TODO: make it a mutable Set instead of a Buffer ?
     */
    private lazy val _vehicles: Buffer[Vehicle] = new ArrayBuffer()

    /** The ingame cities indexed by their names.
     */
    private var _cities: MutMap[String, City] = new HashMap()

    /** Read-only immutable view to the game vehicles.
     */
    def vehicles: Seq[Vehicle] = _vehicles.toSeq

    /** Read-only immutable view to the game trains.
     */
    def trains: Seq[Train] = _vehicles.flatMap({case t: Train => Some(t); case _ => None}).toSeq

    /** Read-only immutable view to the game ships.
     */
    def ships: Seq[Ship] = _vehicles.flatMap({case t: Ship => Some(t); case _ => None}).toSeq

    /** Read-only accessor to the ingame time.
     */
    def time: Integer = _time

    /** Read-only immutable view of the game cities.
     */
    def cities: Map[String, City] = _cities.toMap

    /** Add a new city to the game.
     */
    def addCity(city: City) = {
      _cities.update(city.name, city)
    }

    /** Add a new vehicle to the game.
     */
    def addVehicle(vehicle: Vehicle) = {
      logger.debug(f"Added $vehicle.")
      _vehicles += vehicle
    }

    /** Remove a vehicle from the game.
     */
    def removeVehicle(vehicle: Vehicle) = {
      _vehicles -= vehicle
      logger.debug(f"Removed $vehicle.")
    }

    /** Progress the game by one tick (i.e. one hour)
     */
    def update() = {
        if (!isPaused()) {
          stats.update(this)
          _vehicles.foreach(_.update(this))
          _cities.foreach(_._2.update(this))
          _time += 1
        }
    }

    /** Pause the game.
     */
    def pause(): Unit = {
        _pauseHandler += 1
    }

    /** Unpause the game.
     */
    def unpause(): Unit = {
        _pauseHandler -= 1
    }

    /** Check if the game is paused.
     */
    def isPaused(): Boolean = {
        _pauseHandler != 0
    }


    def writeXml(): Node = {
      <Game money={money.toString} time={_time.toString}>
        {cities.map(_._2).map(_.writeXml)}
        <RailNetwork>
          {networks.rail.stations.map(_._1).map(s => <TrainStation city={s}/>)}
          {networks.rail.trackStream.map(t => <RailWay src={t.src.city.name} dst={t.dst.city.name}/>)}
        </RailNetwork>
        <WaterNetwork>
          {networks.water.stations.map(_._1).map(s => <Port city={s}/>)}
          {networks.water.trackStream.map(t => <WaterWay src={t.src.city.name} dst={t.dst.city.name}/>)}
        </WaterNetwork>
        <Trains>
          {trains.map(_.writeXml)}
        </Trains>
        <Ships>
          {ships.map(_.writeXml)}
        </Ships>
      </Game>
    }

}

object Game
  extends XmlDeserializable[Game]
{
    def readXml(xml: Node) = {
      val game = new Game()

      // Load global constants
      game.money = (xml \ "@money").text.toInt
      game._time = (xml \ "@time").text.toInt

      /// CITIES
      (xml \ "City").map(City.readXml).foreach(game.addCity)

      /// NETWORK
      // Rail network
      (xml \ "RailNetwork" \ "TrainStation").map(x => (x \ "@city").text).map(game.cities).foreach(game.networks.rail.addStation)
      (xml \ "RailNetwork" \ "RailWay").foreach(x => game.networks.rail.addTrack((x \ "@src").text, (x \ "@dst").text))
      // Water network
      (xml \ "WaterNetwork" \ "Port").map(x => (x \ "@city").text).map(game.cities).foreach(game.networks.water.addStation)
      (xml \ "WaterNetwork" \ "WaterWay").map(x => game.networks.water.addTrack((x \ "@src").text, (x \ "@dst").text))

      /// VEHICLES
      // Ships
      (xml \ "Ships" \ "Ship").foreach(s => game.addVehicle(Ship.readXml(s, game)))
      // Trains
      (xml \ "Trains" \ "Train").foreach(t => game.addVehicle(Train.readXml(t, game)))

      game
    }
}
