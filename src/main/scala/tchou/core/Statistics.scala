package tchou.core

import scala.collection.immutable.Map
import scala.collection.immutable.HashMap

import org.jfree.data.xy.XYSeries

import tchou.core.eco.cargo.Cargo
import tchou.utils.xml.XmlSerializable
import tchou.utils.xml.XmlDeserializable


class Statistics
  // extends XmlSerializable
{

  val funds = new XYSeries("funds", false, false)
  val soldResources: Map[Cargo, XYSeries] =
      Cargo.instances.map(c => c._2 -> new XYSeries(c._1, false, false)).toMap
  val earnedResources: Map[Cargo, XYSeries] =
      Cargo.instances.map(c => c._2 -> new XYSeries(c._1, false, false)).toMap

  def addSoldResourceTon(time: Integer, cargo: Cargo, amount: Integer) = {
      val series = soldResources(cargo)
      val old_y = series.getY(time - series.getMinX().ceil.toInt)
      val y = if (old_y == null) { 0 } else { old_y.intValue() }
      series.addOrUpdate(time, y + amount)
  }

  def addSoldResourceMoney(time: Integer, cargo: Cargo, money: Integer) = {
      val series = earnedResources(cargo)
      val old_y = series.getY(time - series.getMinX().ceil.toInt)
      val y = if (old_y == null) { 0 } else { old_y.intValue() }
      series.addOrUpdate(time, y + money)
  }

  def update(game: Game) = {
      funds.add(game.time, game.money)
      Cargo.instances.values.foreach(cargo => {
        try {
          soldResources(cargo).add(game.time, 0)
        } catch { case _: Throwable => () }
        try {
          earnedResources(cargo).add(game.time, 0)
        } catch { case _: Throwable => ()}
      })
  }

}
