package tchou.core.eco.cargo

import scala.collection.mutable.HashMap
import scala.xml.Node

import tchou.utils.Named
import tchou.utils.xml.XmlEnum


sealed abstract case class Cargo(
    /** The name of the resource.
     */
    val name: String,

    /** The weight of a single resource unit, in kg.
     */
    val weight: Integer)
  extends Named

class BulkCargo(name: String) extends Cargo(name, 1000)
class LiquidCargo(name: String, weight: Integer) extends Cargo(name, weight)
class BoxedCargo(name: String, weight: Integer) extends Cargo(name, weight)
class LivingCargo(name: String, weight: Integer) extends Cargo(name, weight)

object BulkCargo extends XmlEnum[BulkCargo] {
  def readXml(xml: Node) =
    new BulkCargo((xml \ "@name").text)
}

object BoxedCargo extends XmlEnum[BoxedCargo] {
  def readXml(xml: Node) =
    new BoxedCargo((xml \ "@name").text, (xml \ "@weight").text.toInt)
}

object LivingCargo extends XmlEnum[LivingCargo] {
  def readXml(xml: Node) =
    new LivingCargo((xml \ "@name").text, (xml \ "@weight").text.toInt)
}

object LiquidCargo extends XmlEnum[LiquidCargo] {
  def readXml(xml: Node) =
    new LiquidCargo((xml \ "@name").text, (xml \ "@weight").text.toInt)
}

object Cargo {
  lazy val instances: Map[String, Cargo] = {
    Seq(BulkCargo, LivingCargo, LiquidCargo, BoxedCargo)
      .foldLeft(new HashMap[String, Cargo])(_ ++ _.instances).toMap
  }
}
