package tchou.core.eco

import scala.collection.mutable.{Map => MutMap}
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Buffer
import scala.collection.immutable.Map
import scala.xml.{Node => XmlNode}

import org.graphstream.graph.Node
import uk.me.jstott.jcoord.LatLng

import tchou.core.Game
import tchou.core.network.Station
import tchou.core.eco.cargo.Cargo
import tchou.core.eco.industry.Industry
import tchou.utils.xml.XmlDeserializable
import tchou.utils.xml.XmlEnum
import tchou.utils.xml.XmlSerializable


object Consumable extends XmlEnum[Cargo] {
  def readXml(xml: XmlNode): Cargo =
    Cargo.instances((xml \ "@name").text)
}



class City(
    val name: String,
    val size: Integer,
    val coordinates: LatLng)
  extends XmlSerializable
{

    private lazy val _resources: MutMap[Cargo, Integer] = {
      MutMap(Cargo.instances.map(c => c._2 -> new Integer(0)).toSeq: _*)
    }

    private lazy val _prices: MutMap[Cargo, Integer] = {
      MutMap(_resources.mapValues(t => new Integer(0)).toSeq: _*)
    }

    private val _industries: Buffer[Industry] = new ArrayBuffer()

    /** Access the city latitude.
     */
    def longitude: Double = coordinates.getLng()

    /** Access the city latitude
     */
    def latitude: Double = coordinates.getLat()

    /** Access the city resources as an mmutable view
     */
    def resources: Map[Cargo, Integer] = _resources.toMap

    /** The list of Industry in the city.
     */
    def industries: Seq[Industry] = _industries.toSeq

    /** Compute the distance between two cities.
     *
     *  @param that another City instance
     */
    def distance(that: City): Double = {
        coordinates.distance(that.coordinates)
    }

    /** Add a factory to the city.
     */
    def addIndustry(industry: Industry) = {
      _industries += industry
    }

    /** The buying price of this resource.
     *
     *  TODO: support dynamic pricing.
     */
    def priceBuy(cargo: Cargo): Integer = {
      if (cargo.name == "Human") {
        new Integer(0)
      } else {
        new Integer(1)
      }
    }

    /** The price the City buys this resource.
     *
     *  TODO: support dynamic pricing.
     */
    def priceSell(cargo: Cargo): Integer = {
      new Integer(2)
    }

    /** Buy a resource to this city.
     *
     *  Will diminish city stocks and game money.
     */
    def buyResource(game: Game, cargo: Cargo, amount: Integer) = {
        _resources(cargo) -= amount
        game.money -= priceBuy(cargo) * amount
    }

    /** Sell a resource to this city.
     *
     *  Will replenish city stocks and game money.
     */
    def sellResource(game: Game, cargo: Cargo, amount: Integer) = {
        val price = priceSell(cargo) * amount
        _resources(cargo) += amount
        game.money += price
        game.stats.addSoldResourceTon(game.time, cargo, amount)
        game.stats.addSoldResourceMoney(game.time, cargo, price)
    }

    /** The resources produced by this City.
     */
    def produces: Seq[Cargo] = {
      Cargo.instances("Human") :: _industries.flatMap(f => f.yields.map(_._1)).toList
    }

    /** The resources desired by this City.
     */
    def wants: Seq[Cargo] = {
      Cargo.instances("Human") :: _industries.flatMap(f => f.intakes.map(_._1)).toList ::: Consumable.instances.map(_._2).toList
    }

    /** Make the city progress ingame by one tick.
     */
    def update(game: Game) = {
      val human = Cargo.instances("Human")
      _resources.update(human, _resources(human) +  size / 100000)
      _industries.foreach(f => f.produce(game.time, resources).foreach(r => _resources(r._1) += r._2))
    }

    /** Serialize this city to Xml.
     */
    def writeXml(): XmlNode = {
      <City name={name} size={size.toString} lat={latitude.toString} lng={longitude.toString}>
        {industries.map(i => <Industry name={i.name}/>)}
        {_prices.toIterator.map(p => <Price cargo={p._1.name} price={p._2.toString}/>)}
        {_resources.toIterator.map(r => <Resource cargo={r._1.name} amount={r._2.toString}/>)}
      </City>
    }

}


object City
  extends XmlDeserializable[City] {
    def readXml(xml: XmlNode): City = {

      // Load city constant attributes
      val name = (xml \ "@name").text
      val size = (xml \ "@size").text.toInt
      val lat = (xml \ "@lat").text.toDouble
      val lng = (xml \ "@lng").text.toDouble

      // Instantiate the city
      val city = new City(name, size, new LatLng(lat, lng))

      // Add industries to the city
      (xml \\ "Industry" \\ "@name").foreach(x => city.addIndustry(Industry.instances(x.text)))

      // Update resource prices
      (xml \\ "Price").foreach(node => {
        city._prices.update(Cargo.instances(( node \ "@cargo").text), (node \ "@price").text.toInt)
      })

      // Update available resources
      (xml \\ "Resource").foreach(node => {
        city._resources.update(Cargo.instances((node \ "@cargo").text), (node \ "@amount").text.toInt)
      })

      //

      city
    }
}
