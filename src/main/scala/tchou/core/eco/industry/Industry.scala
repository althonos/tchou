package tchou.core.eco.industry

import scala.collection.mutable.HashMap
import scala.collection.mutable.{Map => MutMap}
import scala.xml.Node

import tchou.core.Game
import tchou.core.eco.cargo.Cargo
import tchou.utils.Named
import tchou.utils.xml.XmlEnum


sealed abstract case class Industry(
    val name: String,
    val intakes: Seq[(Cargo, Integer)],
    val yields: Seq[(Cargo, Integer)])
  extends Named {

  def produce(time: Integer, resources: Map[Cargo, Integer]): Map[Cargo, Integer];
}

class Mine(
    name_ : String,
    ore: Cargo,
    rate: Integer)
  extends Industry(name_, Seq(), Seq((ore, rate))) {
    def produce(time: Integer, resources: Map[Cargo, Integer]): Map[Cargo, Integer] = {
      yields.toMap
    }
}

class Farm(
    name_ : String,
    intakes_ : Seq[(Cargo, Integer)],
    yields_ : Seq[(Cargo, Integer)],
    val start: Integer,
    val end: Integer)
  extends Industry(name_, intakes_, yields_) {
    def produce(time: Integer, resources: Map[Cargo, Integer]): Map[Cargo, Integer] = {
      val day = (time / 24) % 365 + 1
      if ((day >= start) && (day <= end) && intakes_.forall(r => resources(r._1) >= r._2)) {
        (intakes.map(r => (r._1, (-r._2: Integer))) ++ yields.map(r => (r._1, r._2))).toMap
      } else {
        Map()
      }
    }
}

class Factory(
    name_ : String,
    intakes_ : Seq[(Cargo, Integer)],
    yields_ : Seq[(Cargo, Integer)])
  extends Industry(name_, intakes_, yields_) {
    def produce(time: Integer, resources: Map[Cargo, Integer]): Map[Cargo, Integer] = {
      if (intakes.forall(r => resources(r._1) >= r._2)) {
          (intakes.map(r => (r._1, (-r._2: Integer))) ++ yields.map(r => (r._1, r._2))).toMap
      } else {
        Map()
      }
    }
}

object Mine extends XmlEnum[Mine] {
  def readXml(xml: Node) =
    new Mine(
      (xml \ "@name").text,
      Cargo.instances((xml \ "@ore").text),
      (xml \ "@rate").text.toInt
    )
}

object Farm extends XmlEnum[Farm] {
  def readXml(xml: Node) = {
    val intakes = (xml \\ "intake").map(n => (Cargo.instances((n \ "@name").text), new Integer((n \ "@count").text)))
    val yields = (xml \\ "yield").map(n => (Cargo.instances((n \ "@name").text), new Integer((n \ "@count").text)))
    new Farm(
      (xml \ "@name").text,
      intakes,
      yields,
      (xml \ "@start").text.toInt,
      (xml \ "@end").text.toInt
    )
  }
}

object Factory extends XmlEnum[Factory] {
  def readXml(xml: Node) = {
    val intakes = (xml \\ "intake").map(n => (Cargo.instances((n \ "@name").text), new Integer((n \ "@count").text)))
    val yields = (xml \\ "yield").map(n => (Cargo.instances((n \ "@name").text), new Integer((n \ "@count").text)))
    new Factory(
      (xml \ "@name").text,
      intakes,
      yields
    )
  }
}

object Industry {
  lazy val instances: Map[String, Industry] = {
    Seq(Mine, Farm, Factory)
      .foldLeft(new HashMap[String, Industry])(_ ++ _.instances).toMap
  }
}
