import scala.collection.immutable.Seq
import scala.collection.immutable.HashMap

import org.scalatest._
import uk.me.jstott.jcoord.LatLng

import tchou.core.Game
import tchou.core.eco.City
import tchou.core.network.Station
import tchou.core.vehicles.Vehicle




class VehicleSpec extends FlatSpec with Matchers {

  val game = new Game()
  val network = game.networks.rail

  val paris = City.readXml(<City name="Paris" size="2244000" lat="48.858093" lng="2.294694"/>)
  val lyon = City.readXml(<City name="Lyon" size="484344" lat="45.750000" lng="4.850000"/>)
  val dijon = City.readXml(<City name="Dijon" size="151212" lat="47.3167" lng="5.0167"/>)

  network.addStation(paris)
  network.addStation(lyon)
  network.addStation(dijon)

  network.addTrack("Paris", "Lyon")
  network.addTrack("Lyon", "Dijon")
  network.addTrack("Dijon", "Paris")

  class TestVehicle(it: Seq[Station]) extends Vehicle(it) {
    val id = "test"
    def capacity() = new HashMap()
    def speed() = 10000
    def cost() = 0
    def network(game: Game) = game.networks.rail
    def writeXml() = {<TestVehicle/>}
  }

  "A Vehicle" should "use the shortest path to go to its destination" in {
    val v = new TestVehicle(Seq(network.stations("Paris"), network.stations("Lyon")))
    v.location should be (network.stations("Paris"))
    v.update(game)
    v.location should be (network.tracks(network.stations("Paris"))(network.stations("Lyon")))
  }

  it should "go back and forth through its itinerary if it is linear" in {
    val v = new TestVehicle(Seq(network.stations("Paris"), network.stations("Lyon"), network.stations("Dijon")))

    v.location should be (network.stations("Paris"))
    v.update(game)
    v.update(game)
    v.location should be (network.stations("Lyon"))
    v.update(game)
    v.update(game)
    v.location should be (network.stations("Dijon"))
    v.update(game)
    v.update(game)
    v.location should be (network.stations("Lyon"))
    v.update(game)
    v.update(game)
    v.location should be (network.stations("Paris"))
    v.update(game)
    v.update(game)
    v.location should be (network.stations("Lyon"))
  }

  it should "cycle through its itinerary if it is cyclic" in {
    val v = new TestVehicle(Seq(
      network.stations("Paris"),
      network.stations("Lyon"),
      network.stations("Dijon"),
      network.stations("Paris")
    ))

    v.location should be (network.stations("Paris"))
    v.update(game)
    v.update(game)
    v.location should be (network.stations("Lyon"))
    v.update(game)
    v.update(game)
    v.location should be (network.stations("Dijon"))
    v.update(game)
    v.update(game)
    v.location should be (network.stations("Paris"))
    v.update(game)
    v.update(game)
    v.location should be (network.stations("Lyon"))
    v.update(game)
    v.update(game)
    v.location should be (network.stations("Dijon"))
  }

}
