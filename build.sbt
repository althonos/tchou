// The simplest possible sbt build file is just one line:

//scalaVersion := "2.12.4"      // It is necessary to use Scala 2.11 as
scalaVersion := "2.11.8"        // graphstream's Scala renderer only runs
fork in run := true             // on this version !

// SETTINGS ===================================================================

name := "tchou"
organization := "fr.ens_cachan.dptinfo.larralde"
version := "0.2.0"

// DEPENDENCIES ===============================================================

// XML parser
libraryDependencies += "org.scala-lang" % "scala-xml" % "2.11.0-M4"

// Easy enumerations
libraryDependencies += "com.beachape" %% "enumeratum" % "1.5.12"

// GUI library
libraryDependencies += "org.scala-lang.modules" %% "scala-swing" % "2.0.1"

// graph library
libraryDependencies += "org.graphstream" % "gs-core" % "1.3"
libraryDependencies += "org.graphstream" % "gs-ui" % "1.3"

// logging management
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.8.0"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

// coordinates calculations
resolvers += "MyGrid" at "http://www.mygrid.org.uk/maven/repository"
libraryDependencies += "uk.org.mygrid.resources.jcoord" % "jcoord" % "1.0"

// plot library
libraryDependencies += "org.jfree" % "jfreechart" % "1.5.0"

// TESTING ====================================================================
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
