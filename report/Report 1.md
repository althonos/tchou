### Martin Larralde

# Programming Project -- 1st part

This report is available online at
https://gitlab.com/althonos/tchou/blob/master/report/Report%201.md

## Implementation

The project is contained in its own Scala namespace (`tchou`) and separates
the business logic and the user interface into two different modules (`tchou.core`
and `tchou.ui`).

The program is built with `sbt` and uses git for version control.

The data layer is thoroughly documented, while the UI module isn't so much.
Since it contains lots of boilerplate code that should be easy to understand
as such. I lacked the time to implement the unit tests as well.

### Data Layer

Great care was made to expose as little mutable variables as possible, since
it is the Scala paradigm to use mostly immutable attributes. As such, even
when classes use mutable collections or variables, they often only expose
a public read-only view of those variables, providing more runtime safety.

Attribute types are exposed as traits (such as `Seq` or `Map`) instead of
concrete types for backed intercompatibility.

The library uses the [`scala-logging`](https://github.com/lightbend/scala-logging)
library to show (a lot of) debug information.

The Scala monadic type `Option` was used when possible, to avoid relying on
`null` references, and as such this program is most likely free of the infamous
`NullPointerException` !

#### Ticks

The main thread runs an infinite loop which is in charge of updating the
game every tick. An actual second equals an ingame hour. The ingame time
is simply stored as an hourly counter.

#### Cities and Tracks

Cities are positioned on the game world using their actual GPS coordinates. The
length of a track equals the distance between its two endpoints. The calculations
are handled by the [JCoord](http://www.jstott.me.uk/jcoord/api/1.1-b/uk/me/jstott/jcoord/LatLng.html)
library.

The `City` and `Track` classes are thought as wrappers of `Node` and `Edge`
objects from the [`graphstream`](http://graphstream-project.org/) library. It
should have been possible to implement them as *subclasses* instead, but the
behaviour of the `graphstream` factories were not so well documented, so I went
with the proxy design pattern.

The `Network` class acts as a global manager of all `City` and `Track`
instances, and as such, wraps an underlying `Graph` object.

Cities generate passengers at the rate of 0.001% of their size each tick.

#### Trains

Each `Train` has a particular engine, a type of passenger carriage, and its
source and destination cities. Engines and carriages are defined as *enumerations*
using the [`enumeratum`](https://github.com/lloydmeta/enumeratum) library to
define a set of engines and carriages at compile time.

The available engines are based off two historic engines:

* the Stephenson's Rocket, one of the earliest English locomotives
* the Pacific 231 G, used by the SNCF during the first half of the 20th century

The two carriages are simply *Standard* and *Double*, carrying 50 and 100
passengers respectively.

Each `Train` has a variable `location` attribute, which can be either a `Track`
or a `City`. The train advances each tick towards its destination if if is on
a track (depending on the speed of its engine), or on the track towards the next
city after unloading and loading new passengers.

Trains earn money when unloading their passengers using the simple tarification
of 1 currency unit by passenger by distance traveled.

When creating a new train, it is given a *human-friendly* identifier instead
of a simple incremental number using a Heroku-style generator (see code
for source reference).

#### Routing

Trains use the Dijkstra algorithm to find the shortest path to their destination.

### GUI

When starting the game (`sbt run`), a new window is opened and the game is
immediately started with the main window. There is no way to restart a game
however, the only way is to restart the program manually. Avoiding this would
require passing a callback to the `GameWindow` and connect it to a *restart*
button in the menu, but was not done because this was not seen as a key feature
for now.

#### Cities Window

A window was implemented as a `JFrame` to display specific informations about
each `City`, such as its coordinates, its inhabitants, and the number of
passengers awaiting a train ride. When changing selection in the list, the
selected node is diplayed on the graph, and when a node is clicked, it will
open the cities window and select the appropriate city. This is done by coupling
the Swing listeners provided by the `gs-ui` library with the selection events
of the trains `JList`.

#### *Missing Feature:* Track window

Although it was part of the specification, the choice was made not to create
a specific window to display information about a particular track, as a track
is only specified by its two endpoints and its length, which are all already
displayed on the main graph view.

#### Trains Window

The trains window show information about each train, changing displayed
information using a listener linked to a timer to update the values twice
a second.

Selected trains can be removed using the $-$ button, and a new train can be
added through a specific window using the $+$ button.

When adding a new train through the dedicated window, it is possible to
select the train engine, the train carriage, the train starting city and
the destination. A specific listener is used to make sure the selected
starting city cannot be selected in the destination cities !


#### Game

The `Game` class holds information about the current game, including the trains
owned by the player and a reference to the railroad network. The default game
is based off a subset of the French national railroad network, using real
GPS coordinates and city sizes.

It could be interesting to allow instanciation of a game using a serialization
format (such as JSON or TOML) instead of hardcoding the values in the `Main`
class.


### Testing

The library lack unit tests, but it uses GitLab continuous integration to
make sure the program builds on any machine. The CI logs are accessible at
[this adress: https://gitlab.com/althonos/tchou/pipelines](https://gitlab.com/althonos/tchou/pipelines)
