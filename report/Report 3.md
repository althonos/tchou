### Martin Larralde

# Programming Project -- 3rd part

This report is available online at
https://gitlab.com/althonos/tchou/blob/master/report/Report%203.md

## Implementation

### Data Layer

#### Fixes

The code was patched to prevent the following bugs to occur: when a vehicle
had more capacity than available cargo, it would still load fully and leave
negative storage in the city.

#### Refactor

The trait `XmlSerializable` was renamed `XmlDeserializable` (as objects implementing
it can be *loaded* from XML), and a new `XmlSerializable` trait was created (for
objects that can be *saved* to XML).

Some attributes of the `Vehicle` class were changed from `private` to
`protected` to allow XML serialization in their specialized subclasses.

The `GameToolbar` widget was refactored to implement the `ActionListener` trait
and match each event source as one of its components, instead of defining
an anonymous listeners for each individual button. The same pattern was applied
to `GameMenuBar`.

#### Saves

Games can be saved using an XML format that is actually a superset of the
map format. They contain the following information:

* Cities:
  - name, latitude, longitude, size
  - industries
  - price of each resource
  - storage level of each resource
* Networks (Rail & Water):
  - Stations
  - Tracks
* Vehicles - Trains:
  - ID
  - Engine
  - current location, roadmap, itinerary
  - current cargo levels
* Vehicles - Ships:
  - ID
  - Ship class & transported resource
  - current location, roadmap, itinerary
  - current cargo levels

A game can be loaded from a savegame using the `readXml` method of the
`Game` companion object. An example save is available is the
`src/test/resources/saves` directory at the root of the project.

#### Statistics

Statistics are collected by an *updatable* attribute of the game itself,
accessible as `game.stats`. The following statistics are collected until the
game is exited, or another game is loaded:

* The company funds over time
* The amount of resources transported, per resource
* The money earned per resource

The backend library used to create plots and charts is *JFreeChart*. Just as
*GraphStream* manages both the logic and the rendering of graphs, *JFreeChart*
allows the rendering of dynamic XY plots within a Swing interface. Although it
could have been possible to reduce the coupling between GUI and data-layer by
using a library-agnostic container to hold statistics data, there is no proper
way in the *JFreeChart* API to simply wrap the underlying storage, meaning it
would have been necessary to keep rebuilding an intermediate storage container
simply for rendering.

Note that the statistics are **not** saved when saving a game. Also note that
there is a **known bug** about resource-specific statistics not being updated
after a game is loaded; fixing it would probably require to change the current
storage logic of the collected data (as `XYSeries` are only updatable through
a fudge used here).

### GUI

#### Saves

Saves can be created and loaded using the `File > Save...` and `File > Load...`
items of the main window menu bar. While a save window is opened, the game
will be paused. Once the save is successfully created or loaded, a *popup*
will appear and the game will restart after it is closed.

Saving is done through the standard Swing file chooser, so users are free
to save their saves wherever they want, and *unfortunately*, with the name
they want. A save will be shown in the `File > Load...` dialog only if it has
the `.xml` extension, using the `XmlFilter` class in `SaveChooser.scala`.

A more user-friendly experience would consist in a dedicated window with a list
of available saves, allowing to create, delete, or load saves without exposing
the underlying storage. Saves could be for instance located in a dedicated
folder in the `XDG_DATA_HOME` (i.e. `~/.local/share` on most UNIXes).

#### Game window

Since games can now be loaded, the `GameWindow` class allows mutating its
`game` attribute, which will trigger most of the interface to rebuild around
the new `Game` instance. This can lead to some unsatisfying behaviour: for
instance when loading a game, the old cities / vehicles windows will stay
open and need to be closed and reopened manually to display information about
the new game.

#### Statistics window

A new button was added to the toolbar, allowing to open the *statistics*
window. The window consists in three panes:

* The company funds over time
* The amount of resources transported, per resource
* The money earned per resource

The *per-resource* plots are configured to stack, so that when money is earned
from several resources at the same time, it is easy to see how much each
resource contributed to the total:

![](cargo.png)

Currently, the two last panes look the same because the game does not implement
a dynamic economy system, meaning the money earned by resource stays
proportional to the amount of transported resource over time. However, were a
such a system implemented, it would be easy to render on the
