### Martin Larralde

# Programming Project -- 2nd part

This report is available online at
https://gitlab.com/althonos/tchou/blob/master/report/Report%202.md

## Implementation

Most of the implementation was reused after the previous part, as it was designed
to be scalable, and proved to be (for at least some parts).

### Data Layer

#### Maps

Maps are serialized using XML serialization. Most of the attributes are references
to other objects which instances are defined in XML files (implementing `XMLEnum`).
This serialization framework is used in many places, so that configuring and
balancing the game does not require source code recompilation. (Such a game
could even be considered to support moding, as new industries and vehicle types
can be added).

#### Cities

Since there are now more than one means of transportation, `City` objects were
decoupled with nodes of each transportation network. As such, a `City` is now
an object that wraps one or more `Station` which are each nodes on their
respective transportation network: for instance, *Le Havre* has both a `Port`
and a `TrainStation` as both transportation means are available there.

I didn't have time (and, to be honest, ideas) to manage dynamic resource cost,
and since I didn't balance resources properly it would probably have been a
pain to setup. So for now, all resources cost exactly 1 when they are bought,
and 2 when they are sold.

#### Trains

`Train` now have more than one carriages; each carriage has a specific resource
it can carry. The number of carriages a train can haul is not limited; however,
the cost to operate the train, as well as the speed the train can go (it will
never go less than 1 km/h fast to ensure trains can actually reach their
destination). Engines are left unchaged.

#### Ships

Ships are part of a specific class, and each class can carry a specific resource
only, defined when the ship is created. Ships have increased capacity compared
to trains but can only carry a specific resource.

#### Planes

Planes were not added by lack of time, but following the `Vehicle` framework
introduced by this version they should be pretty straightforward to implement,
were they needed later.

#### Resource Trading

When reaching a station, a vehicle will determine if it should sell its
carried resources and buy new ones:

* if the station is not part of the vehicle itinerary (it's only a step on
  its travel), then no resource exchange will occur.
* if the station is part of the vehicle itinerary, the vehicle will sell all
  resources that are wanted by the city (either consumed by the city, or
  used as intake by one of the city industries).
* if the station is part of the vehicle itinerary, the vehicle will buy all
  resources that are produced by the city.

Passengers are treated as a special resource, that are wanted by all cities,
and produced by all cities. They also do not have a *buy price* (when transporting
passenger, no money is spent when they enter the vehicle and money is earneds when
they leave the vehicle).

### GUI

#### Cities Window

The cities window was not changed that much, since the only new things to
be listed are factories and stations for each city.

Each city now has a popup listing the resource stock for the current city.

#### Vehicle Window

The Train window was given a tad more abstraction to display different vehicle
types in different tabs, while keeping the same behaviour in all panes. It was
renamed to `VehicleWindow`

##### New Trains

Since trains can now have an itinerary that consists of more than two cities,
a new widget was created to allow selecting an itinerary. When creating an
itinerary, it is only possible to select the next city from a list of cities
that are connected to the last city of the itinerary. It is therefore impossible
to create an itinerary between cities that are part of the same network while
not being connected by any edges.

The train creation window also offer the possibility to select a list of carriages
from the carriages instances defined in `cariages.xml`.

When creating a new train, the creation will proceed only when the itinerary
contains at least two cities.

The same old simple combo box allows selecting the train engine.

##### New Ships

Ships also use the itinerary selection widget. A combo box allows selecting
the boat class. When a class is selected, it will set the values from the
resources combo box, that allow selecting which resource the ship will transport.

#### Game

As there are now more than one transportation means, there are also more than
one transportaton networks, that are rendered independently in several different
tabs.

Vehicles are now rendered on the game view, using a little trick: they are added
as nodes on the transportation network graph, and assigned a special *vehicle*
class that allows specific styling. This makes the Graphstream library render
them without complications. Position of each vehicle is updated manually using
a timer.
