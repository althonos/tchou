NEEDED:



MAYBE:

* Add a proper interface for saves instead of relying on a file chooser
* Avoid game starting directly
* Better money
* Add prices


IDEAS:

- Engine type
- Variable train speed depending on charge
- Train composition (number and type of wagons)
- Maximum speed per railway
- Different resources instead of only passengers
